/*
Navicat MySQL Data Transfer

Source Server         : 阿里云数据
Source Server Version : 50729
Source Host           : 47.96.156.23:3306
Source Database       : farmhouse

Target Server Type    : MYSQL
Target Server Version : 50729
File Encoding         : 65001

Date: 2020-04-14 23:59:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `admin`
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` varchar(32) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'li', '1wweXUTeipFQ65Hs/1Y1eA==');

-- ----------------------------
-- Table structure for `cart`
-- ----------------------------
DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart` (
  `cart_id` varchar(32) NOT NULL,
  `commodity_id` varchar(32) NOT NULL,
  `number` int(10) DEFAULT '0',
  `cart_state` int(1) DEFAULT '0' COMMENT '购物车商品状态：0:加入，1：已购买，2：已删除',
  `create_time` datetime(6) DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(6) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`cart_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='购物车';

-- ----------------------------
-- Records of cart
-- ----------------------------

-- ----------------------------
-- Table structure for `child_order`
-- ----------------------------
DROP TABLE IF EXISTS `child_order`;
CREATE TABLE `child_order` (
  `child_order_id` varchar(32) NOT NULL,
  `order_id` varchar(32) NOT NULL COMMENT '订单id',
  `commodity_id` varchar(32) NOT NULL COMMENT '商品ID',
  `commodity_number` int(10) DEFAULT '0' COMMENT '商品数量',
  `commodity_price` decimal(10,2) DEFAULT '0.00' COMMENT '商品原价',
  `transaction_price` decimal(10,2) DEFAULT NULL COMMENT '成交价格',
  `create_time` datetime(6) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`child_order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单商品表';

-- ----------------------------
-- Records of child_order
-- ----------------------------

-- ----------------------------
-- Table structure for `commodity`
-- ----------------------------
DROP TABLE IF EXISTS `commodity`;
CREATE TABLE `commodity` (
  `id` varchar(32) NOT NULL,
  `shop_id` varchar(32) DEFAULT NULL COMMENT '商家ID',
  `commodity_title` varchar(200) DEFAULT NULL COMMENT '商品标题',
  `commodity_type_id` varchar(32) DEFAULT NULL COMMENT '商品类别ID',
  `images_group_id` varchar(32) DEFAULT NULL COMMENT '图片组ID',
  `create_time` date DEFAULT NULL COMMENT '创建时间',
  `is_shelves` int(1) DEFAULT '0' COMMENT '是否下架：0否，1是',
  `price` decimal(10,0) DEFAULT NULL,
  `inventory` int(10) DEFAULT '0' COMMENT '库存',
  `salesCount` int(11) DEFAULT '0' COMMENT '销量',
  `description` varchar(500) DEFAULT NULL COMMENT '描述',
  `is_discount` int(1) DEFAULT '0' COMMENT '是否打折 0否，1是',
  `discount_price` decimal(10,0) DEFAULT NULL COMMENT '折扣价格',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品表';

-- ----------------------------
-- Records of commodity
-- ----------------------------
INSERT INTO `commodity` VALUES ('123456789', null, '苹果', null, null, null, '0', '11', '40', '0', '11111', '0', null);
INSERT INTO `commodity` VALUES ('4c371d5e37c6b1638e2ea94eb59c4eb7', null, '新增商品测试', '1', null, null, '0', '13', '100', '0', null, '0', null);
INSERT INTO `commodity` VALUES ('c4e2b3a6b2ef84bba7c3fa1f46e2cb98', null, '新增商品测试2', '1', null, null, '0', '14', '100', '0', null, '0', null);

-- ----------------------------
-- Table structure for `commodity_type`
-- ----------------------------
DROP TABLE IF EXISTS `commodity_type`;
CREATE TABLE `commodity_type` (
  `type_id` varchar(32) NOT NULL,
  `shop_id` varchar(32) DEFAULT NULL,
  `type_title` varchar(200) DEFAULT NULL COMMENT '类型标题',
  `type_description` varchar(500) DEFAULT NULL COMMENT '类型描述',
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of commodity_type
-- ----------------------------
INSERT INTO `commodity_type` VALUES ('0e853f9f2224e6b842c838f2c8c58abf', '6be43d456d72e1105235c8486bf42d54', '测试修改类别标题', '测试修改描述');
INSERT INTO `commodity_type` VALUES ('2cea220572342335ca2c216a1b1b005a', '6be43d456d72e1105235c8486bf42d54', '测试类别标题6', '测试描述1');
INSERT INTO `commodity_type` VALUES ('3ab713ac63467597a69b508303b5fdc1', '6be43d456d72e1105235c8486bf42d54', '测试类别标题', '测试描述');
INSERT INTO `commodity_type` VALUES ('3c4ee4c54199636ea9b0248b4fdf40ff', '1244653691154833', '6', '6777');
INSERT INTO `commodity_type` VALUES ('54129440665ffc8593739d6d9073273f', '1244653691154833', '8888', '8888');
INSERT INTO `commodity_type` VALUES ('5b47034d21d1309ab407b8997a24695d', '1244653691154833', '测试2', '测试2');
INSERT INTO `commodity_type` VALUES ('60c18aa2995276d3bb83cae7aefc23cb', '1244653691154833', '测试4', '测试4');
INSERT INTO `commodity_type` VALUES ('77ad91d8a754b187ee7efbda556dbd77', '1244653691154833', '8', '8');
INSERT INTO `commodity_type` VALUES ('862a8aa0137801d4ed21c6928d5dac11', '6be43d456d72e1105235c8486bf42d54', '测试类别标题4', '测试描述1');
INSERT INTO `commodity_type` VALUES ('965cc1af33bf2967d2e35ccf44ef42d1', '6be43d456d72e1105235c8486bf42d54', '测试类别标题7', '测试描述1');
INSERT INTO `commodity_type` VALUES ('a27a7a465ed1dbc6006cd68837ac4ae3', '6be43d456d72e1105235c8486bf42d54', '测试类别标题9', '测试描述1');
INSERT INTO `commodity_type` VALUES ('b2721efd1e6455345be39d784d38c67f', '6be43d456d72e1105235c8486bf42d54', '测试类别标题2', '测试描述1');
INSERT INTO `commodity_type` VALUES ('b61029ba1d412c516a52a508f6778055', '6be43d456d72e1105235c8486bf42d54', '测试类别标题8', '测试描述1');
INSERT INTO `commodity_type` VALUES ('c8bf30713de3f8fda5cac4d5936464ac', '1244653691154833', '测试', '测试');
INSERT INTO `commodity_type` VALUES ('ea797607382c36c72088dfd134565497', '1244653691154833', '测试3', '测试3');
INSERT INTO `commodity_type` VALUES ('f1f0c529271494857d86d0884d52f958', '1244653691154833', '7', '7');

-- ----------------------------
-- Table structure for `images`
-- ----------------------------
DROP TABLE IF EXISTS `images`;
CREATE TABLE `images` (
  `image_id` varchar(32) NOT NULL,
  `group_id` varchar(32) DEFAULT NULL COMMENT '组ID',
  `image_name` varchar(100) DEFAULT NULL COMMENT '图片名称',
  `image_url` varchar(200) DEFAULT NULL COMMENT '图片路径',
  `image_order` int(1) DEFAULT '1' COMMENT '图片顺序',
  `image_type` int(1) DEFAULT NULL COMMENT '图片类别：1.商品图片，2.农家院图片',
  `shop_id` varchar(32) DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='图片表';

-- ----------------------------
-- Records of images
-- ----------------------------
INSERT INTO `images` VALUES ('1ee5cf9dc1adcd1e22837a04b0b39375', null, 'cc59b306-406e-4070-8efb-e8bffb0bbf81.png', '/farmhouse/upload/image/', '1', null, null);
INSERT INTO `images` VALUES ('c0b410841091a1690db38e6d2844e5b7', null, '2beef673-cae6-4911-8ebd-545051c96edd.png', '/farmhouse/upload/image/', '1', null, null);

-- ----------------------------
-- Table structure for `order`
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order` (
  `order_id` varchar(32) NOT NULL COMMENT '订单ID',
  `order_code` varchar(32) NOT NULL COMMENT '订单编号',
  `shop_id` varchar(32) NOT NULL COMMENT '商家ID',
  `user_id` varchar(32) NOT NULL COMMENT '用户ID',
  `commodity_number` int(10) DEFAULT '0' COMMENT '商品数量',
  `total_money` decimal(10,2) DEFAULT '0.00' COMMENT '订单money',
  `order_state` int(1) DEFAULT '0' COMMENT '订单状态：0待付款，1已付款，2取消付款，3完成交易，4退款',
  `payment_time` datetime(6) DEFAULT NULL COMMENT '支付时间',
  `refund_time` datetime(6) DEFAULT NULL COMMENT '退款时间',
  `create_time` datetime(6) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单表';

-- ----------------------------
-- Records of order
-- ----------------------------

-- ----------------------------
-- Table structure for `shop`
-- ----------------------------
DROP TABLE IF EXISTS `shop`;
CREATE TABLE `shop` (
  `id` varchar(32) NOT NULL,
  `create_time` datetime(6) NOT NULL,
  `update_user` varchar(32) DEFAULT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(200) NOT NULL COMMENT '账户密码',
  `shop_name` varchar(200) DEFAULT NULL COMMENT '店铺名称',
  `shop_owner` varchar(50) DEFAULT NULL COMMENT '店主真实名称',
  `id_card` varchar(50) DEFAULT NULL COMMENT '身份证号',
  `address` varchar(500) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL COMMENT '联系电话1',
  `phone2` varchar(20) DEFAULT NULL COMMENT '联系电话2',
  `state` int(1) DEFAULT '0' COMMENT '店铺审核状态 0.待审核，1.审核通过',
  `shop_status` int(1) DEFAULT '0' COMMENT '店铺状态：0.打烊，1.营业中',
  `notice` varchar(500) DEFAULT NULL COMMENT '店铺公告',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商家表';

-- ----------------------------
-- Records of shop
-- ----------------------------
INSERT INTO `shop` VALUES ('1244653691154833', '2020-03-30 15:50:31.796000', '1', '2020-04-11 08:59:35.868000', 'li', '4QrcOUm6Wau+VuBX8g+IPg==', 'li的店铺', '李成兰', '500234199603051311', '重庆市南岸区茶园新区水云路xx号', '13896925249', null, '2', '0', '这是一条测试公告内容2');
INSERT INTO `shop` VALUES ('1244963348951957', '2020-03-31 12:22:47.118000', '1', '2020-04-11 08:59:43.437000', 'z', '+63p42o/NtPWdsG4CEUd1w==', 'z', null, null, null, null, null, '2', '0', null);
INSERT INTO `shop` VALUES ('1245609250596683', '2020-04-02 07:09:20.927000', '1', '2020-04-11 09:02:21.099000', 'wdfs', '/OqSD3QStdp74M9CuMk3WQ==', '北京信息科技大学', null, null, null, null, null, '1', '0', null);
INSERT INTO `shop` VALUES ('1245615750287294', '2020-04-02 07:35:11.946000', null, null, 'qwer', '/OqSD3QStdp74M9CuMk3WQ==', '万华酒店', null, null, null, null, null, '0', '0', null);
INSERT INTO `shop` VALUES ('1245672695610617', '2020-04-02 11:21:28.578000', null, null, 'lili', 'RspivozqM6IKwgGvsimd0Q==', 'li店铺', null, null, null, null, null, '0', '0', null);
INSERT INTO `shop` VALUES ('1245981497849085', '2020-04-03 07:48:31.842000', null, null, 'zxy123', '4QrcOUm6Wau+VuBX8g+IPg==', 'zxy123', '张鑫宇', null, null, null, null, '0', '0', '张鑫宇账号测试');
INSERT INTO `shop` VALUES ('1245991478099394', '2020-04-03 08:28:12.434000', null, '2020-04-04 08:27:12.376000', 'qianshan', 'z0JG5ukt+bou8ffgNzPOCQ==', '千山暮雪', '千山暮雪', '500234199603051111', '测试地址', '13896925249', '13896925249', '0', '0', '比如说是其他的公告');
INSERT INTO `shop` VALUES ('1246018067482681346', '2020-04-03 10:13:51.838000', null, null, 'ceshi11', 'zbLbnIvXDAFL4O023AWjYw==', 'ceshi11', null, null, null, null, null, '0', '0', null);
INSERT INTO `shop` VALUES ('1246018098172403714', '2020-04-03 10:13:59.159000', null, null, 'ceshi12', 'QAijYL9feCCHgrSU/t7jnQ==', 'ceshi12', null, null, null, null, null, '0', '0', null);
INSERT INTO `shop` VALUES ('1246018149909143553', '2020-04-03 10:14:11.494000', null, null, 'ceshi13', 'MG+RwhQrPtSXCBbn6CpC6g==', 'ceshi13', null, null, null, null, null, '0', '0', null);
INSERT INTO `shop` VALUES ('1246018183920754690', '2020-04-03 10:14:19.601000', null, null, 'ceshi14', 'di/la+BU1y4FASczHHEbgA==', 'ceshi14', null, null, null, null, null, '0', '0', null);
INSERT INTO `shop` VALUES ('1246018216514691074', '2020-04-03 10:14:27.374000', null, null, 'ceshi15', 'YpzvWNAft9+bFMPcHRtn+g==', 'ceshi15', null, null, null, null, null, '0', '0', null);
INSERT INTO `shop` VALUES ('3d6ba3c7928dce3ca3b48cbc5c2b6e14', '2020-04-04 11:13:59.080000', null, null, 'ceshi16', 'kZ6yiIFGXtkqVhS7sk3NHA==', 'ceshi16', null, null, null, null, null, '0', '0', null);
INSERT INTO `shop` VALUES ('d0cc64e2d23a4aa05950792e26451215', '2020-04-12 05:27:16.123000', null, null, 'lwadun', 'TSbh49Gr6bsvetaSQ1aoJQ==', 'A123', null, null, null, null, null, '0', '0', null);

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` varchar(50) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `gender` int(5) DEFAULT NULL,
  `user_email` varchar(200) DEFAULT NULL,
  `age` int(5) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('0469b6e4b22d1acfb8cac6d9b5318d7c', 'lyj', '4QrcOUm6Wau+VuBX8g+IPg==', '2020-04-13 01:45:42', '0', '1615987@qq.com', '45', '13366320310');
INSERT INTO `user` VALUES ('14d646ac613a586bde7031c859df39b1', '戴先东', '4QrcOUm6Wau+VuBX8g+IPg==', '2020-04-10 06:48:37', '0', '1615987554@qq.com', '4', '110');
INSERT INTO `user` VALUES ('1a1b37e2139b6de265d9326d4a9d858d', '张三', 'xMpCOKC5I4INzFCab3WEmw==', '2020-04-10 06:54:14', '8', '', '5', '');
INSERT INTO `user` VALUES ('1a7f31e46f6d40f0b7b754616bb4f304', '张三', '4QrcOUm6Wau+VuBX8g+IPg==', '2020-04-10 06:49:56', '8', '', '5', '');
INSERT INTO `user` VALUES ('1cc193e24234bba34c5a7b71a14c', '张三', '4QrcOUm6Wau+VuBX8g+IPg==', '2020-04-10 02:19:28', '1', '', '15', '');
INSERT INTO `user` VALUES ('2', '张三', '123456', null, null, '', null, '');
INSERT INTO `user` VALUES ('26758999c4cff259365e533348e83200', 'zxy', '4QrcOUm6Wau+VuBX8g+IPg==', '2020-04-10 12:47:41', '1', '123@qq.com', '45', '13366320310');
INSERT INTO `user` VALUES ('34508877d5f2ac14fde12a7b99124436', 'zhangsan', 'JdVa0oOqQAr0ZMdtcTwHrQ==', '2020-04-10 13:59:45', '0', '1615987@qq.com', '45', '13366320310');
INSERT INTO `user` VALUES ('5fc6ba6069f70222d35769f548d06a22', '张三', 'xMpCOKC5I4INzFCab3WEmw==', '2020-04-10 06:33:26', '1', '', '45', '');
INSERT INTO `user` VALUES ('63d9dd6082e00ab6ce98708cb5180760', '张三', 'JfnnlDI7RTiF9RgfG2JNCw==', '2020-04-10 07:21:56', '1', '', '45', '');
INSERT INTO `user` VALUES ('64e2f2dcba05c61b30945d1620513594', '张三', 'gnzLDuqKcGxMNKFokfhOew==', '2020-04-10 06:49:17', '1', '', '45', '');
INSERT INTO `user` VALUES ('66fbb97ae2031db32a3e221b03bed40d', '张三', 'f6goKtkwR6TW/mERyTswig==', '2020-04-10 07:14:22', '1', '', '45', '');
INSERT INTO `user` VALUES ('749efe49cf566c8d5dc9877a53674562', '张三', '4QrcOUm6Wau+VuBX8g+IPg==', '2020-04-10 06:48:48', '1', '', '45', '');
INSERT INTO `user` VALUES ('b19b88641c3b5ec8b9dd9d1c28182233', '张三', '1wweXUTeipFQ65Hs/1Y1eA==', '2020-04-07 14:34:01', '1', '', '24', '');
INSERT INTO `user` VALUES ('c302e61965ca576ca3bcd11026e23b55', '张三', '4QrcOUm6Wau+VuBX8g+IPg==', '2020-04-10 06:33:04', '1', '', '5', '');
INSERT INTO `user` VALUES ('caa22a455a3c8ff28a469c8e0c66156b', '张三', '/OqSD3QStdp74M9CuMk3WQ==', '2020-04-10 07:06:50', '1', '', '45', '');
INSERT INTO `user` VALUES ('cc667a1652e2d59390e5ce5f23860fc0', '张三', 'DMF1ucDxtqgxw5niaXcmYQ==', '2020-04-10 07:06:09', '8', '', '5', '');
INSERT INTO `user` VALUES ('d942b027d4c087799b27a97479ce0af4', '赵六', '123456', '2020-04-09 10:42:29', '1', '', '15', '');
INSERT INTO `user` VALUES ('fe29aef74428f93dca6ea5ba2212cdd5', '张三', '4QrcOUm6Wau+VuBX8g+IPg==', '2020-04-10 06:31:07', '1', '', '5', '');
INSERT INTO `user` VALUES ('ff5b44e7dd854a0d31fbad0fa02bfc39', '张三', 'xMpCOKC5I4INzFCab3WEmw==', '2020-04-10 06:52:45', '8', '', '5', '');
