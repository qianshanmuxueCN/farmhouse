package com.farmhouse.demo.entity;

import lombok.Data;

/**
 * @author 千山暮雪
 * @title: CommodityHouseDTO
 * @projectName demo
 * @description: TODO
 * @date 2020/5/12 0:03
 */
@Data
public class CommodityHouseDTO {
    /**
     * 商品类别ID
     */
    private String commodityTypeId;
    private String shopId;
    private int pageNum;
    private int size;
}
