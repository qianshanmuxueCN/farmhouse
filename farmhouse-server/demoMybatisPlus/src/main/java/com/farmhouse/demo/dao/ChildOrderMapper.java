package com.farmhouse.demo.dao;

import com.farmhouse.demo.entity.ChildOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 订单商品表 Mapper 接口
 * </p>
 *
 * @author 千山暮雪
 * @since 2020-04-14
 */
@Mapper
public interface ChildOrderMapper extends BaseMapper<ChildOrder> {

}
