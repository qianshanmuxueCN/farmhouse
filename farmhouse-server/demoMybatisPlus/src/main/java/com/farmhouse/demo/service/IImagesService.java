package com.farmhouse.demo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.farmhouse.demo.entity.Images;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author 千山暮雪
 * @since 2020-04-03
 */
public interface IImagesService extends IService<Images> {

    /**
     * @param imageName
     * @param shopId
     * @return String
     * @description: 新增图片接口
     * @date 2020/4/13 22:57
     */
    String addImage(String imageName, String shopId);

    /**
     * @return IPage<Images>
     * @description: 分页查询图片列表
     * @date 2020/4/14 17:09
     */
    IPage<Images> getListByPage(Images images, IPage<Images> page);

    /**
     * @param images
     * @return String
     * @description: 修改图片信息
     * @date 2020/4/14 17:34
     */
    String updateImageInfo(Images images);

    /**
     * @param imageId
     * @param shopId
     * @return boolean
     * @description: 删除图片
     * @date 2020/4/14 17:43
     */
    boolean deleteImage(String imageId, String shopId);

    /**
     * @description: 绑定商品
     * @date 2020/5/14 20:58
     */
    boolean bindImage(String shopId, String imageId, String groupId, Integer imageOrder);
}
