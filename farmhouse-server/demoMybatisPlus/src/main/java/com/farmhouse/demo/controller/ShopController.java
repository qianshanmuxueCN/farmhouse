package com.farmhouse.demo.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.farmhouse.demo.entity.CommonResult;
import com.farmhouse.demo.entity.ShopEntity;
import com.farmhouse.demo.entity.ShopEntityDTO;
import com.farmhouse.demo.service.ShopService;
import com.farmhouse.demo.utils.MD5Util;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


@Api("商家相关的接口")
@Slf4j
@RestController
@RequestMapping("/shop")
public class ShopController {
    @Autowired
    private ShopService shopService;
    @Autowired
    private HttpServletRequest request;

    /**
     * @param username
     * @param password
     * @return CommonResult
     * @description: 商家登录方法
     * @date 2020/3/30 21:59
     */

    @PostMapping("/login")
    public CommonResult<ShopEntity> login(String username, String password) {
        //数据校验
        if (!StringUtils.isEmpty(username) && !StringUtils.isEmpty(password)) {
            //调用服务
            ShopEntity result = shopService.login(username, password);
            //打印日志
            if (result != null) {
                request.getSession().setAttribute("userName", username);
                request.getSession().setAttribute("userId", result.getId());
            }
            log.info("{}用户登录结果：{}", username, !ObjectUtils.isEmpty(result));
            //返回结果给前端
            return ObjectUtils.isEmpty(result) ? new CommonResult<ShopEntity>(303, "用户名或者密码错误！") :
                    new CommonResult<ShopEntity>(200, "登录成功！", result);
        } else {
            log.info("{}用户登录失败", username);
            return new CommonResult<ShopEntity>(300, "参数有误！");
        }
    }

    /**
     * @param username
     * @param password
     * @param shopName
     * @return CommonResult
     * @description: 商家注册方法
     * @date 2020/3/30 21:59
     */
    @PostMapping("/register")
    public CommonResult<ShopEntity> register(String username, String password, String shopName) {
        //数据校验
        if (!StringUtils.isEmpty(username) && !StringUtils.isEmpty(password) && !StringUtils.isEmpty(shopName)) {
            //调用服务
            ShopEntity result = shopService.register(shopName, username, MD5Util.encoderByMd5(password));
            //打印日志
            log.info("{}商家用户注册结果：{}", username, !ObjectUtils.isEmpty(result));
            //返回结果给前端
            return ObjectUtils.isEmpty(result) ? new CommonResult<ShopEntity>(303, "注册失败！店铺名称或者账户名已存在") :
                    new CommonResult<ShopEntity>(200, "注册成功！", result);
        } else {
            log.info("{}商家用户注册失败", username);
            return new CommonResult<ShopEntity>(300, "参数有误！");
        }
    }

    /**
     * @param shopEntity
     * @return CommonResult
     * @description: 商家完善商户信息的接口
     * @date 2020/3/30 21:59
     */
    @PostMapping("/updateInfo")
    public CommonResult<ShopEntity> updateInfo(@RequestBody ShopEntity shopEntity) {
        if (!ObjectUtils.isEmpty(shopEntity) && !ObjectUtils.isEmpty(shopEntity.getId())) {
            ShopEntity entity = shopService.updateInfo(shopEntity);
            log.info("{}商家用户修改商家用户信息，参数为{}", shopEntity.getUsername(), shopEntity.toString());
            return ObjectUtils.isEmpty(entity) ? new CommonResult<ShopEntity>(304, "修改失败") :
                    new CommonResult<ShopEntity>(200, "修改成功", entity);
        } else {
            log.info("商家用户注册失败：参数有误");
            return new CommonResult<ShopEntity>(300, "参数有误！");
        }
    }

    /**
     * @param id
     * @return CommonResult
     * @description: 根据ID查询商铺信息
     * @date 2020/3/30 23:21
     */
    @GetMapping("/getShopEntityById")
    public CommonResult<ShopEntity> getShopEntityById(String id) {
        if (!ObjectUtils.isEmpty(id)) {
            ShopEntity shopEntity = shopService.getShopEntityById(id);
            return ObjectUtils.isEmpty(shopEntity) ? new CommonResult<ShopEntity>(304, "查询失败！") :
                    new CommonResult<ShopEntity>(200, "查询成功", shopEntity);
        } else {
            return new CommonResult<ShopEntity>(300, "参数有误！");
        }
    }

    /**
     * @param shopEntityDTO
     * @return CommonResult
     * @description: 根据条件分页查询商铺信息
     * @date 2020/3/30 23:21
     */
    @GetMapping("/getShopList")
    public CommonResult getByWrapper(ShopEntityDTO shopEntityDTO) {
        IPage<ShopEntity> page = new Page<>();
        page.setCurrent(shopEntityDTO.getPageNum()); //当前页
        page.setSize(shopEntityDTO.getSize());    //每页条数
        IPage<ShopEntity> shopEntityIPage = shopService.getByWrapper(new ShopEntity(shopEntityDTO), page);
        return ObjectUtils.isEmpty(shopEntityIPage) && ObjectUtils.isEmpty(shopEntityIPage.getRecords())
                ? new CommonResult<ShopEntity>(304, "查询失败！")
                : new CommonResult<IPage<ShopEntity>>(200, "查询成功", shopEntityIPage);
    }

    /**
     * @param id
     * @param notice
     * @return CommonResult
     * @description: 商家发布公告
     * @date 2020/3/31 0:17
     */
    @PostMapping("/updateNotice")
    public CommonResult updateNotice(String id, String notice) {
        if (!ObjectUtils.isEmpty(id) && !StringUtils.isEmpty(notice)) {
            int i = shopService.updateNotice(id, notice);
            return i == 1
                    ? new CommonResult(200, "更新公告成功")
                    : new CommonResult(304, "更新公告失败！");
        } else {
            return new CommonResult(304, "参数异常！");
        }
    }

    /**
     * @param shopId
     * @param adminId
     * @param result
     * @return CommonResult
     * @description: 管理员审核商家注册状态
     * @date 2020/4/1 21:40
     */
    @PostMapping("/auditShopState")
    public CommonResult auditShopState(String shopId, String adminId, int result) {
        if (!ObjectUtils.isEmpty(shopId) && !ObjectUtils.isEmpty(adminId) && !ObjectUtils.isEmpty(result)) {
            int i = shopService.auditShopState(shopId, adminId, result);
            log.info("{}审核商家{}的注册状态为{}", adminId, shopId, result);
            return i == 1
                    ? new CommonResult(200, "审核操作成功")
                    : new CommonResult(304, "审核操作失败！");
        } else {
            return new CommonResult(317, "参数有误！");
        }
    }

    /**
     * @param shopId
     * @param status
     * @return CommonResult
     * @description: 商家修改自己店铺的状态
     * @date 2020/4/1 22:07
     */
    @PostMapping("/updateShopStatus")
    public CommonResult updateShopStatus(String shopId, int status) {
        if (!ObjectUtils.isEmpty(shopId) && !ObjectUtils.isEmpty(status)) {
            int i = shopService.updateShopStatusByShopOwner(shopId, status);
            log.info("{}修改商家{}的店铺状态为{}", shopId, status);
            return i == 1
                    ? new CommonResult(200, "修改成功")
                    : new CommonResult(304, "修改失败！");
        } else {
            return new CommonResult(317, "参数有误！");
        }
    }

    /**
     * @param shopId
     * @param adminId
     * @return CommonResult
     * @description: 管理员重置商家登录密码
     * @date 2020/4/1 22:13
     */
    @PostMapping("/resetPasswordByAdmin")
    public CommonResult resetPasswordByAdmin(String shopId, String adminId) {
        if (!ObjectUtils.isEmpty(shopId) && !ObjectUtils.isEmpty(adminId)) {
            int i = shopService.resetPasswordByAdmin(shopId);
            log.info("{}重置商家商家密码为123456", "admin");
            return i == 1
                    ? new CommonResult(200, "重置成功", "123456")
                    : new CommonResult(304, "重置失败！");
        } else {
            return new CommonResult(317, "参数有误！");
        }
    }

    /**
     * @param shopId
     * @param oldPassword
     * @param newPassword
     * @return CommonResult
     * @description: 修改密码
     * @date 2020/4/1 22:26
     */
    @PostMapping("/updatePassword")
    public CommonResult updatePassword(String shopId, String oldPassword, String newPassword) {
        if (!ObjectUtils.isEmpty(shopId) && !StringUtils.isEmpty(oldPassword) && !StringUtils.isEmpty(newPassword)) {
            int i = shopService.updatePassword(shopId, oldPassword, newPassword);
            log.info("商家修改密码为{}", newPassword);
            return i == 1
                    ? new CommonResult(200, "修改成功")
                    : new CommonResult(304, "修改失败！");
        } else {
            return new CommonResult(317, "参数有误！");
        }
    }
}
