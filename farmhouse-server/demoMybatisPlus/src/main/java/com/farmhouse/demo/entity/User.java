package com.farmhouse.demo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.util.ObjectUtils;

import java.io.Serializable;
import java.util.Date;
/**
 *UUID 32位UUID字符串
 */
@Data
@Builder
@TableName("user")
//链式编程
@Accessors(chain = true)
@AllArgsConstructor
public class User implements Serializable {
    @TableId(type = IdType.UUID)
    private String  id;
    private String username;
    private String password;
    private String phone;
    private Integer gender;//用户性别，0是女，1是男
    private String userEmail;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date createTime;//创建日期
    private Integer age;

    public User(){

    }
//trim()功能:除去字符串开头和末尾的空格或其他字符
    public User(UserDTO userDTO){
        this.username = ObjectUtils.isEmpty(userDTO.getUsername()) ? null : userDTO.getUsername().trim();
        this.phone    = ObjectUtils.isEmpty(userDTO.getPhone())    ? null : userDTO.getPhone().trim();
        this.gender=ObjectUtils.isEmpty(userDTO.getGender())?        null:  userDTO.getGender();
        this.userEmail=ObjectUtils.isEmpty(userDTO.getUserEmail())?null:userDTO.getUserEmail();
    }
}
