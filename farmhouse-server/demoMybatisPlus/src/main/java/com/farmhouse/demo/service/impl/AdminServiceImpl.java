package com.farmhouse.demo.service.impl;



/*
 * QueryMapper条件查询构造器，先构造好条件，然后再去查询
 *
 * */

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.farmhouse.demo.dao.AdminMapper;
import com.farmhouse.demo.entity.Admin;
import com.farmhouse.demo.service.AdminService;
import com.farmhouse.demo.utils.MD5Util;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import javax.annotation.Resource;

@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements AdminService {
    @Resource
    private AdminMapper adminDao;

    @Override
    public Admin login(String username, String password) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("username", username);
        Admin one = getOne(queryWrapper);
        if (!ObjectUtils.isEmpty(one) && MD5Util.checkpassword(password,one.getPassword())) {
            one.setPassword("");
            return one;
        } else {
            return null;
        }
    }

}
