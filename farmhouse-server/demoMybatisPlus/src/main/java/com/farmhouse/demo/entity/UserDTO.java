package com.farmhouse.demo.entity;

import lombok.Data;

import java.util.Date;

/**
 * @author 千山暮雪
 * @title: UserDTO
 * @projectName: farmhouse-server,用户实体传输对象
 * @description: 用户上传实体类
 * @date 2020/04/15/14:06
 */
@Data
public class UserDTO {
    private String username;
    private String phone;
    private Integer gender;
    private Date createTime;
    private String userEmail;
    private int pageNum;//页码的数量
    private int size;//每页显示的数量
}
