package com.farmhouse.demo.controller;

import com.farmhouse.demo.entity.CommonResult;
import com.farmhouse.demo.service.IImagesService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import org.apache.commons.io.FileUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * @author 千山暮雪
 * @title: PictureUploadController
 * @projectName demo
 * @description: 图片上传接口
 * @date 2020/4/4 23:19
 */
@Api("图片上传接口")
@RestController
@RequestMapping("/pictureUpload")
@Slf4j
public class PictureUploadController {

    @Value("${upload-images-path}")
    private String imgPath;

    @Autowired
    private IImagesService iImagesService;
    @Autowired
    private HttpServletRequest request;
    /**
     * @param files     图片文件
     * @return CommonResult
     * @description: 上传图片的接口
     * @date 2020/4/13 22:12
     */
    @PostMapping("/uploadImages")
    public CommonResult uploadImages(@RequestParam(value = "file") MultipartFile files[]) {
        log.info("图片上传存储路径:{},groupId:{},imageType:{}", imgPath);
        String shopId = (String) request.getSession().getAttribute("userId");
        File uploadDirectory = new File(imgPath);
        if (uploadDirectory.exists()) {
            if (!uploadDirectory.isDirectory()) {
                uploadDirectory.delete();
            }
        } else {
            uploadDirectory.mkdir();
        }
        //这里可以支持多文件上传
        List<String> idList =new ArrayList<>();
        if (files != null && files.length >= 1) {
            BufferedOutputStream bw = null;
            try {

                for (MultipartFile file : files) {
                    String fileName = file.getOriginalFilename();
                    //判断是否有文件且是否为图片文件
                    if (fileName != null && !"".equalsIgnoreCase(fileName.trim()) && isImageFile(fileName)) {
                        //创建输出文件对象
                        String imageName =UUID.randomUUID().toString() + getFileType(fileName);
                        File outFile = new File(imgPath + imageName);
                        //拷贝文件到输出文件对象
                        FileUtils.copyInputStreamToFile(file.getInputStream(), outFile);
                        idList.add(iImagesService.addImage(imageName,shopId));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (bw != null) {
                        bw.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        Map<String,Object> map = new HashMap();
        map.put("ids", idList);
        return new CommonResult(200, "上传成功", map);
    }

    /**
     * @param fileName
     * @return Boolean
     * @description: 判断是否是图片
     * @date 2020/4/4 23:35
     */
    private Boolean isImageFile(String fileName) {
        String[] img_type = new String[]{".jpg", ".jpeg", ".png", ".gif", ".bmp"};
        if (fileName == null) {
            return false;
        }
        fileName = fileName.toLowerCase();
        for (String type : img_type) {
            if (fileName.endsWith(type)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 获取文件后缀名
     *
     * @param fileName
     * @return
     */
    private String getFileType(String fileName) {
        if (fileName != null && fileName.indexOf(".") >= 0) {
            return fileName.substring(fileName.lastIndexOf("."), fileName.length());
        }
        return "";
    }
}
