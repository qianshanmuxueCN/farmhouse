package com.farmhouse.demo.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 订单商品表 前端控制器
 * </p>
 *
 * @author 千山暮雪
 * @since 2020-04-14
 */
@RestController
@RequestMapping("/order/child-order")
public class ChildOrderController {

}
