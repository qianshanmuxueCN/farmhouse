package com.farmhouse.demo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.farmhouse.demo.entity.ShopEntity;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface ShopMapper extends BaseMapper<ShopEntity> {

}
