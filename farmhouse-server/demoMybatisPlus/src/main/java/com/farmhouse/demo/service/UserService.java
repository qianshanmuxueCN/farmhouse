package com.farmhouse.demo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.farmhouse.demo.entity.User;

public interface UserService extends IService<User> {

    User login(String userName, String password);

    User register(String username, String password, String phone, int gender, String userEmail, int age);
  //根据id更改用户信息
    User updateInfo(User user);
  //根据id获取用户信息
    User getUserById(String id);
    //用户修改密码
    int updatePassword(String id, String oldPassword, String newPassword);
    //用户重置密码
    int resetPassword(String username, String phone, String userEmail);
    //分页查询用户信息
    IPage<User> getByWrapper(User user, IPage page);
   //根据用户id删除用户
  int deleteById(String id);

   int resetPasswordById(String id,String adminId);
}
