package com.farmhouse.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.farmhouse.demo.entity.Images;
import com.farmhouse.demo.dao.ImagesMapper;
import com.farmhouse.demo.service.IImagesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 千山暮雪
 * @since 2020-04-03
 */
@Service
public class ImagesServiceImpl extends ServiceImpl<ImagesMapper, Images> implements IImagesService {
    @Resource
    private ImagesMapper imagesMapper;
    @Value("${upload-images-url}")
    private String imgUrl;

    @Override
    public String addImage(String imageName, String shopId) {

        Images images = new Images();
        images.setImageName(imageName).setImageUrl(imgUrl).setShopId(shopId);
        if (save(images)) {
            QueryWrapper<Images> imagesQueryWrapper = new QueryWrapper<>();
            imagesQueryWrapper.eq("image_name", shopId);
            Images one = getOne(imagesQueryWrapper);
            if (!ObjectUtils.isEmpty(one))
                return one.getImageId();
        }
        return null;
    }

    @Override
    public IPage<Images> getListByPage(Images images, IPage<Images> page) {
        QueryWrapper<Images> queryWrapper = new QueryWrapper<Images>();
        queryWrapper.eq("shop_id", images.getShopId());
        if (!StringUtils.isEmpty(images.getImageName())) {
            queryWrapper.like("image_name", images.getImageName());
        }
        if (!StringUtils.isEmpty(images.getImageType())) {
            queryWrapper.like("image_type", images.getImageType());
        }
        return imagesMapper.selectPage(page, queryWrapper);
    }

    @Override
    public String updateImageInfo(Images images) {
        UpdateWrapper<Images> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("image_type", images.getImageType())
                .set("group_id", images.getGroupId())
                .set("image_name", images.getImageName())
                .set("image_order", images.getImageOrder());
        updateWrapper.eq("image_id", images.getImageId()).eq("shop_id", images.getShopId());
        return update(updateWrapper) ? "修改成功" : "修改失败";
    }

    @Override
    public boolean deleteImage(String imageId, String shopId) {
        UpdateWrapper<Images> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("group_id", null)
                .set("shop_id", null);
        updateWrapper.eq("image_id", imageId).eq("shop_id", shopId);
        return update(updateWrapper);
    }

    @Override
    public boolean bindImage(String shopId, String imageId, String groupId, Integer imageOrder) {
        UpdateWrapper updateWrapper = new UpdateWrapper();
        updateWrapper.set("group_id", groupId);
        updateWrapper.set("image_order", imageOrder);
        updateWrapper.eq("shop_id", shopId);
        updateWrapper.eq("image_id", imageId);
        return update(updateWrapper);
    }
}
