package com.farmhouse.demo.entity;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单商品表
 * </p>
 *
 * @author 千山暮雪
 * @since 2020-04-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ChildOrder implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId
    private String childOrderId;

    /**
     * 父订单编号
     */
    @TableField("f_order_code")
    private String orderCode;

    /**
     * 商品ID
     */
    private String commodityId;

    /**
     * 商品数量
     */
    private Integer commodityNumber;

    /**
     * 商品原价
     */
    private BigDecimal commodityPrice;

    /**
     * 成交价格
     */
    private BigDecimal transactionPrice;

    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date createTime;


}
