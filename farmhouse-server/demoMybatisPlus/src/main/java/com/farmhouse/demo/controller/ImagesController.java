package com.farmhouse.demo.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.farmhouse.demo.entity.CommonResult;
import com.farmhouse.demo.entity.Images;
import com.farmhouse.demo.entity.ImagesDTO;
import com.farmhouse.demo.service.IImagesService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 图片前端控制器
 * </p>
 *
 * @author 千山暮雪
 * @since 2020-04-03
 */
@Api(value = "图片接口")
@RestController
@RequestMapping("/commodity/images")
public class ImagesController {

    @Autowired
    private IImagesService imagesService;
    @Autowired
    private HttpServletRequest request;

    /**
     * @param imagesDTO
     * @return CommonResult<IPage < Images>>
     * @description: 分页查询图片列表
     * @date 2020/4/14 17:29
     */
    @GetMapping("/getListByPage")
    public CommonResult<IPage<Images>> getListByPage(ImagesDTO imagesDTO) {
        IPage<Images> page = new Page();
        Images images = new Images(imagesDTO);
        String shopId = (String) request.getSession().getAttribute("userId");
        if (StringUtils.isEmpty(shopId)) {
            return new CommonResult(304, "无权限");
        }
        images.setShopId(shopId);
        page.setCurrent(imagesDTO.getPageNum()); //当前页
        page.setSize(imagesDTO.getSize());    //每页条数
        IPage<Images> listByPage = imagesService.getListByPage(images, page);
        return new CommonResult<IPage<Images>>(200, "查询成功", listByPage);
    }

    /**
     * @param images
     * @return CommonResult
     * @description: 修改图片信息 ：绑定图片至商品或者是店铺
     * @date 2020/4/14 17:46
     */
    @PostMapping("/updateImageInfo")
    public CommonResult updateImageInfo(Images images) {
        String shopId = (String) request.getSession().getAttribute("userId");
        if (!StringUtils.isEmpty(shopId) && shopId.equals(images.getShopId())) {
            return new CommonResult(200, imagesService.updateImageInfo(images));
        } else {
            return new CommonResult(403, "权限不足");
        }
    }

    /**
     * @param imageId
     * @return CommonResult
     * @description: 删除图片：但是实际操作是解除绑定关系
     * @date 2020/4/14 17:47
     */
    @PostMapping("/deleteImage")
    public CommonResult deleteImage(String imageId) {
        String shopId = (String) request.getSession().getAttribute("userId");
        boolean t = imagesService.deleteImage(imageId, shopId);
        return t ? new CommonResult(200, "删除成功") : new CommonResult(401, "删除失败");
    }

    @PostMapping("/bindImage")
    public CommonResult bindImage(String imageId, String groupId, Integer imageOrder) {
        String shopId = (String) request.getSession().getAttribute("userId");
        if (StringUtils.isEmpty(shopId))
            return new CommonResult(403, "权限不足");
        if (StringUtils.isEmpty(imageId)||StringUtils.isEmpty(groupId)||StringUtils.isEmpty(imageOrder))
            return new CommonResult(403, "参数错误");
        return new CommonResult(200,"绑定成功",imagesService.bindImage(shopId,imageId,groupId,imageOrder));
    }
}
