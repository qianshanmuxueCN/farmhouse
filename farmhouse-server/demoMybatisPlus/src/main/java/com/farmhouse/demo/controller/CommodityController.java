package com.farmhouse.demo.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Assert;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.farmhouse.demo.entity.*;
import com.farmhouse.demo.service.ICommodityService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * * <p>
 * * 商品前端控制器
 * * </p>z
 * *
 * * @author 千山暮雪
 * * @since 2020-04-03
 */
@Api(value = "商品相关的接口")
@RestController
@RequestMapping("/commodity/commodity")
public class CommodityController {

    @Autowired
    private ICommodityService commodityService;
    @Autowired
    private HttpServletRequest request;

    /**
     * 分页获取商品列表
     *
     * @param commodityDTO
     * @return
     */
    @GetMapping("/getListByPage")
    @ResponseBody
    public CommonResult<IPage<Commodity>> manage(CommodityDTO commodityDTO) {
        String shopId = (String) request.getSession().getAttribute("userId");
        IPage<Commodity> page = new Page<>();
        page.setCurrent(commodityDTO.getPageNum()); //当前页
        page.setSize(commodityDTO.getSize());    //每页条数
        IPage<Commodity> commodityIPage = commodityService.getByPage(new Commodity(commodityDTO), page, shopId);
        return new CommonResult<IPage<Commodity>>(200, "查询成功", commodityIPage);
    }

    /**
     * 创建商品
     *
     * @author
     * @date 2020/04/05 01:45
     */
    @PostMapping("/create")
    @ResponseBody
    public CommonResult<Boolean> create(Commodity Commodity) {
        String shopId = (String) request.getSession().getAttribute("userId");
        if (StringUtils.isEmpty(shopId)) {
            return new CommonResult<>(403, "请重新登录");
        } else {
            Commodity.setShopId(shopId)
                    .setInventory(1)
                    .setCreateTime(new Date());
        }
        return new CommonResult(200, "添加成功", commodityService.save(Commodity));
    }

    /**
     * 删除商品
     *
     * @author
     * @date 2020/04/05 01:45
     */
    @PostMapping("/delete")
    @ResponseBody
    public CommonResult deleteById(String commodityId, String shop) {
        String shopId = (String) request.getSession().getAttribute("userId");
        if (!StringUtils.isEmpty(shopId) && shopId.equals(shop)) {
            commodityService.deleteById(commodityId);
            return new CommonResult(200, "删除成功");
        } else {
            return new CommonResult(401, "删除失败");
        }

    }

    /**
     * 修改商品
     *
     * @author
     * @date 2020/04/05 01:45
     */
    @PostMapping("/updateById")
    @ResponseBody
    public CommonResult updateById(Commodity Commodity) {
        String shopId = (String) request.getSession().getAttribute("userId");
        if (!StringUtils.isEmpty(shopId)) {
            Assert.notNull(Commodity.getId(), "缺少商品ID");
            commodityService.update(Commodity);
            return new CommonResult(200, "修改成功");
        } else {
            return new CommonResult(403, "无权限");
        }
    }

    /**
     * 商品上下架
     * isShelves：1 下架 ；0 上架
     *
     * @date 2020/04/05 01:45
     */
    @PostMapping("/shelves")
    @ResponseBody
    public CommonResult shelves(String commodityId, Integer isShelves) {
        if (StringUtils.isEmpty(commodityId))
            return new CommonResult(417, isShelves == 1 ? "下架失败" : "上架失败" + ":商品ID为空");
        return commodityService.shelves(commodityId, isShelves) ?
                new CommonResult(200, isShelves == 1 ? "下架成功" : "上架成功") :
                new CommonResult(403, isShelves == 1 ? "下架失败" : "上架失败");
    }

    /**
     * 根据ID查询商品
     *
     * @author
     * @date 2020/04/05 01:45
     */
    @GetMapping("/{id}")
    @ResponseBody
    public CommonResult<Commodity> finById(@PathVariable String id) {
        return new CommonResult(200, "查询成功", commodityService.getById(id));
    }

    /**
     * @description: 商品打折接口
     * isDiscount:是否打折 1：是；0：否
     * @date 2020/4/9 0:52
     */
    @PostMapping("/discount")
    public CommonResult discount(@NotNull String commodityId, int isDiscount, Double price) {
        boolean discount = commodityService.discount(commodityId, isDiscount, price);
        return new CommonResult(200, "保存成功");
    }

    /**
     * @description: 库存管理
     * @date 2020/4/11 18:29
     */
    @PostMapping("/inventory")
    public CommonResult inventory(@NotNull String commodityId, int inventory) {
        boolean inventory1 = commodityService.inventory(commodityId, inventory);
        return new CommonResult(200, "库存修改成功");
    }

    /**
     * @return ${return_type}
     * @description: 分页获取农家大院的商品列表
     * @date 2020/5/11 17:38
     */
    @GetMapping("/getShopHousePage")
    public CommonResult<IPage<ShopHouseVO>> getShopHousePage(ShopHouseDTO shopHouseDTO) {
        IPage<ShopHouseVO> page = new Page<>();
        page.setCurrent(shopHouseDTO.getPageNum()); //当前页
        page.setSize(shopHouseDTO.getSize());    //每页条数
        IPage<ShopHouseVO> shopHousePage = commodityService.getShopHousePage(page, shopHouseDTO.getShopName());
        return new CommonResult<>(200, "", shopHousePage);
    }

    /**
     * @return CommonResult
     * @description: 分页获取农家大院的商品列表
     * @date 2020/5/12 00:12
     */
    @GetMapping("/getCommodityByShop")
    public CommonResult<IPage<Commodity>> getCommodityByShop(CommodityHouseDTO commodityDTO) {
        if (StringUtils.isEmpty(commodityDTO))
            return new CommonResult<>(304, "店铺数据错误");
        IPage<Commodity> page = new Page<>();
        page.setCurrent(commodityDTO.getPageNum()); //当前页
        page.setSize(commodityDTO.getSize());    //每页条数
        IPage<Commodity> commodityPage = commodityService.getCommodityByShop(page, commodityDTO.getShopId(), commodityDTO.getCommodityTypeId());
        return new CommonResult<>(200, "", commodityPage);
    }

    /**
     * @description: 根据商家获取商品列表
     * @date 2020/5/14 20:40
     */
    @GetMapping("/getListByShop")
    public CommonResult<List<Commodity>> getListByShop() {
        String shopId = (String) request.getSession().getAttribute("userId");
        if(StringUtils.isEmpty(shopId))
            return new CommonResult<>(403,"请登录");
        return new CommonResult<>(200,"查询成功",commodityService.getListByShop(shopId));
    }
}
