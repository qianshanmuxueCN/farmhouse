package com.farmhouse.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.farmhouse.demo.entity.Admin;
/*
* IService
*
* */



public interface AdminService extends IService<Admin> {
    /**
     * @param username
     * @param password
     * @return Admin
     * @description: admin登录
     */
    Admin login(String username, String password);
}
