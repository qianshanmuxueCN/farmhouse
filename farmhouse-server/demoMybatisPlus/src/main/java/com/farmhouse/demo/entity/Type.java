package com.farmhouse.demo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.util.ObjectUtils;

/**
 * <p>
 *
 * </p>
 *
 * @author 千山暮雪
 * @since 2020-04-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("commodity_type")
public class Type implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId(type = IdType.UUID)
    private String typeId;

    private String shopId;

    /**
     * 类型标题
     */
    private String typeTitle;

    /**
     * 类型描述
     */
    private String typeDescription;

    public Type() {
    }

    public Type(String typeId, String shopId, String typeTitle, String typeDescription) {
        this.typeId = typeId;
        this.shopId = shopId;
        this.typeTitle = typeTitle;
        this.typeDescription = typeDescription;
    }

    public Type(TypeDTO typeDTO) {
        this.shopId = ObjectUtils.isEmpty(typeDTO.getShopId()) ? null : typeDTO.getShopId().trim();
        this.typeTitle = ObjectUtils.isEmpty(typeDTO.getTypeTitle()) ? null : typeDTO.getTypeTitle().trim();
    }
}
