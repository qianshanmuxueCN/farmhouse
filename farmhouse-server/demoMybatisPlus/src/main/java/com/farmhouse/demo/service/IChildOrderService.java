package com.farmhouse.demo.service;

import com.farmhouse.demo.entity.ChildOrder;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单商品表 服务类
 * </p>
 *
 * @author 千山暮雪
 * @since 2020-04-14
 */
public interface IChildOrderService extends IService<ChildOrder> {

}
