package com.farmhouse.demo.entity;

import lombok.Data;

/**
 * @author 千山暮雪
 * @title: CommodityDTO
 * @projectName demo
 * @description: s商品上传对象
 * @date 2020/4/5 2:03
 */
@Data
public class CommodityDTO {

    /**
     * 商品标题
     */
    private String commodityTitle;

    /**
     * 商品类别ID
     */
    private String commodityTypeId;

    /**
     * 是否打折 0否，1是
     */
    private Integer isDiscount;

    private int pageNum;
    private int size;
}
