package com.farmhouse.demo.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.farmhouse.demo.entity.Cart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.farmhouse.demo.entity.CartVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 购物车 Mapper 接口
 * </p>
 *
 * @author 千山暮雪
 * @since 2020-04-14
 */
@Mapper
public interface CartMapper extends BaseMapper<Cart> {

    /**
     * @description: 加载购物车列表数据
     * @date 2020/5/12 17:47
     */
    List<CartVO> selectListByUser(String userId);
    /**
        * @description: 设置用户购物车中的商品状态
        * @date 2020/5/13 1:37
        */
    int finishCart(String userId,String[] cartIds);
}
