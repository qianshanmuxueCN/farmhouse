package com.farmhouse.demo.service.impl;

import com.farmhouse.demo.entity.ChildOrder;
import com.farmhouse.demo.dao.ChildOrderMapper;
import com.farmhouse.demo.service.IChildOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 订单商品表 服务实现类
 * </p>
 *
 * @author 千山暮雪
 * @since 2020-04-14
 */
@Service
public class ChildOrderServiceImpl extends ServiceImpl<ChildOrderMapper, ChildOrder> implements IChildOrderService {

    @Resource
    private ChildOrderMapper childOrderMapper;

}
