package com.farmhouse.demo.config;

import com.farmhouse.demo.interceptor.SessionInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author 千山暮雪
 * @title: SessionConfiguration
 * @projectName farmhouse-server
 * @description: 拦截器的配置
 * @date 2020/4/2 15:07
 */
@Configuration
public class SessionConfiguration implements WebMvcConfigurer {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/farmhouse/index/html/index.html").setViewName("userIndex");
        registry.addViewController("/").setViewName("forward:/farmhouse/index/html/index.html");
        registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new SessionInterceptor())
                //网站配置生成器：添加一个拦截器，拦截路径为整个项目
                .excludePathPatterns("/**")

                /*.addPathPatterns("/**")*/
                .excludePathPatterns("**/login.html")
                .excludePathPatterns("/html/**")
                .excludePathPatterns("/admin/login")
                .excludePathPatterns("/shop/login")
                .excludePathPatterns("/shop/register")
                .excludePathPatterns("/error")
                .excludePathPatterns("/index/**");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/js/", "/css/", "/images/**")
                .addResourceLocations("classpath:/index/res/")
                .addResourceLocations("classpath:/html/js/")
                .addResourceLocations("classpath:/html/css/")
                .addResourceLocations("classpath:/html/lib/")
                .addResourceLocations("classpath:/html/fonts/")
                .addResourceLocations("classpath:/html/images/");
    }

}
