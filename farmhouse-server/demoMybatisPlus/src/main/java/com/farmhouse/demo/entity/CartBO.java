package com.farmhouse.demo.entity;

import lombok.Data;

/**
 * @author 千山暮雪
 * @title: CartBO
 * @projectName demo
 * @description: TODO
 * @date 2020/5/13 0:05
 */
@Data
public class CartBO {
    //商品数据
    private String commodityId;
    private Double price;//价格
    private Integer isDiscount;//是否打折 0否，1是
    private Double discountPrice;//折扣价格
    private Integer inventory;//库存
    private Integer isShelves;//是否下架：0否，1是

    //购物车数据
    private String cartId;
}
