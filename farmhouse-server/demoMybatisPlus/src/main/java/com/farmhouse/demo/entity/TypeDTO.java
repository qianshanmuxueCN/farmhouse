package com.farmhouse.demo.entity;

import lombok.Data;

/**
 * @author 千山暮雪
 * @title: TypeDTO
 * @projectName demo
 * @description: TODO
 * @date 2020/4/7 22:02
 */
@Data
public class TypeDTO {
    private String shopId;

    /**
     * 类型标题
     */
    private String typeTitle;

    private int pageNum;
    private int size;
}
