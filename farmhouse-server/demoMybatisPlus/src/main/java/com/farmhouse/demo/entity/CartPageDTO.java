package com.farmhouse.demo.entity;

import lombok.Data;

/**
 * @author 千山暮雪
 * @title: CartPageDTO
 * @projectName demo
 * @description: TODO
 * @date 2020/5/12 15:12
 */
@Data
public class CartPageDTO {
    //商品名称
    private String commodityName;
    private int pageNum;
    private int size;
}
