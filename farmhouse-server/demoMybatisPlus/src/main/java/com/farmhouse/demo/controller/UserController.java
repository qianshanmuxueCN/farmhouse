package com.farmhouse.demo.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.farmhouse.demo.entity.CommonResult;
import com.farmhouse.demo.entity.User;
import com.farmhouse.demo.entity.UserDTO;
import com.farmhouse.demo.service.UserService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Api("普通用户相关的接口")
@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

    @Autowired
    public UserService userService;
    @Autowired
    public HttpServletRequest request;

    @PostMapping("/login")
    public CommonResult<User> login(String username, String password) {
        if (!StringUtils.isEmpty(username) && !StringUtils.isEmpty(password)) {
            User result = userService.login(username, password);
            if (result != null) {
                request.getSession().setAttribute("userName", username);
                request.getSession().setAttribute("userId", result.getId());
            }
            return ObjectUtils.isEmpty(result) ? new CommonResult<User>(303, "用户名或密码错误") :
                    new CommonResult<User>(200, "登陆成功", result);
        } else {

            return new CommonResult<User>(300, "参数有误");
        }
    }

    /***
     *
     * 用户注册
     * @return
     */
    @PostMapping("/register")
    public CommonResult<User> register(String username, String password, String phone, int gender, String userEmail, int age) {
        if (!StringUtils.isEmpty(username) && !StringUtils.isEmpty(password)) {
            User result = userService.register(username, password, phone, gender, userEmail, age);
            request.getSession().setAttribute("userName", username);
            return ObjectUtils.isEmpty(result) ? new CommonResult<User>(303, "注册失败,用户已经存在") :
                    new CommonResult<User>(200, "注册成功");
        } else {
            return new CommonResult<User>(300, "参数有误,请重试");
        }
    }

    /**
     * @description: 修改用户信息
     * @date 2020/4/9 21:41
     */
    @PostMapping("/updateInfoById")
    public CommonResult<User> updateInfoById(User user) {
        if (!ObjectUtils.isEmpty(user) && !ObjectUtils.isEmpty(user.getId())) {
            User result = userService.updateInfo(user);
            return ObjectUtils.isEmpty(result) ? new CommonResult<User>(304, "更新失败") :
                    new CommonResult<User>(200, "更新成功");
        } else {
            return new CommonResult<User>(300, "参数错误！");
        }
    }

    @GetMapping("/getUserById")
    public CommonResult<User> getUserById(String id) {
        if (!ObjectUtils.isEmpty(id)) {
            User result = userService.getUserById(id);
            return ObjectUtils.isEmpty(result) ? new CommonResult<User>(303, "获取用户信息失败") :
                    new CommonResult<User>(200, "获取用户信息成功", result);
        } else {
            return new CommonResult<User>(300, "参数错误");
        }
    }

    /***
     * 用户修改密码
     * @param id
     * @param oldPassword
     * @param newPassword
     * @return
     */
    @PostMapping("/updatePassword")
    public CommonResult updatePassword(String id, String oldPassword, String newPassword) {
        if (!ObjectUtils.isEmpty(id) && !StringUtils.isEmpty(oldPassword) && !StringUtils.isEmpty(newPassword)) {
            int result = userService.updatePassword(id, oldPassword, newPassword);
            return ObjectUtils.isEmpty(result) ? new CommonResult(303, "修改失败") :
                    new CommonResult(200, "修改成功");
        } else {
            return new CommonResult(303, "参数错误");
        }
    }

    /***
     * 用户重置密码
     * @param username 用户名
     * @param phone 手机号
     * @param userEmail email
     * @return CommonResult
     */
    @PostMapping("/resetPassword")
    public CommonResult resetPassword(String username, String phone, String userEmail) {
        if (!StringUtils.isEmpty(username) && !StringUtils.isEmpty(phone) && !StringUtils.isEmpty(userEmail)) {
            int result = userService.resetPassword(username, phone, userEmail);
            return result == 1 ? new CommonResult(200, "重置成功") :
                    new CommonResult(303, "重置失败");
        } else {
            return new CommonResult(303, "参数有误");
        }
    }

    /***
     * 获取用列表
     * @param userDTO
     * @return
     */

    @GetMapping("/getUserList")
    public CommonResult<IPage<User>> getByWrapper(UserDTO userDTO) {
        IPage<User> iPage = new Page<>();
        //设置当前页数
        iPage.setCurrent(userDTO.getPageNum());
        //每页条数
        iPage.setSize(userDTO.getSize());
        IPage<User> userIPage = userService.getByWrapper(new User(userDTO), iPage);
        return ObjectUtils.isEmpty(userIPage) && ObjectUtils.isEmpty(iPage.getRecords()) ?
                new CommonResult(304, "查询失败")//ERROR 报错 warning
                : new CommonResult<IPage<User>>(200, "查询成功", userIPage);
    }

    /***
     * 根据id删除用户
     * @param id
     * @return
     */
    @PostMapping("/deleteUser")
    public CommonResult deleteUser(String id, String adminId) {
        String admin = (String) request.getSession().getAttribute("userId");
        if (!StringUtils.isEmpty(id) && !StringUtils.isEmpty(adminId) && adminId.equals(admin)) {
            int result = userService.deleteById(id);
            return result == 1 ? new CommonResult(200, "删除成功") :
                    new CommonResult(401, "删除失败");
        } else {
            return new CommonResult(401, "权限不足");
        }
    }

    /**
     * 根据用户id重置密码
     *
     * @param id
     * @param adminId
     * @return
     */
    @PostMapping("/resetPasswordById")
    public CommonResult<User> resetPasswordById(String id, String adminId) {
        String admin = (String) request.getSession().getAttribute("userId");
        if (!StringUtils.isEmpty(id) && !StringUtils.isEmpty(adminId) && adminId.equals(admin)) {
            int result = userService.resetPasswordById(id, adminId);
            return result == 1 ? new CommonResult<User>(200, "重置成功") :
                    new CommonResult<User>(303, "重置失败");
        }
        return new CommonResult<User>(303, "重置失败2");
    }
}