package com.farmhouse.demo.utils;

import lombok.extern.slf4j.Slf4j;
import sun.misc.BASE64Encoder;

import java.security.MessageDigest;

/**
 * @author
 * @title: MD5
 * @projectName farmhouse-server
 * @description: MD5工具类
 * @date 2020/3/23 22:26
 */
@Slf4j
public class MD5Util {
    /**
     * 利用MD5进行加密
     */
    public static String encoderByMd5(String str) {
        String value = null;
        try {
            //确定计算方法
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            BASE64Encoder base64en = new BASE64Encoder();
            //加密后的字符串
            value = base64en.encode(md5.digest(str.getBytes("utf-8")));

        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }
        return value;
    }

    /**
     * 判断用户密码是否正确
     * plaintext 用户输入的密码
     * ciphertext 正确密码
     */
    public static boolean checkpassword(String plaintext, String ciphertext) {
        try {
            if (encoderByMd5(plaintext).equals(ciphertext))
                return true;
            else
                return false;
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(encoderByMd5("li"));
//        System.out.println(checkpassword("l2i","1wweXUTeipFQ65Hs/1Y1eA=="));
    }

}
