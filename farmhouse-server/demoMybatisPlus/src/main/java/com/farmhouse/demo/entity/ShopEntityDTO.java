package com.farmhouse.demo.entity;

import lombok.Data;

import java.util.Date;

/**
 * @author 千山暮雪
 * @title: ShopEntityDTO，商铺实体传输对象
 * @projectName farmhouse-server
 * @description: shopDTO
 * @date 2020/3/31 0:07
 */
@Data
public class ShopEntityDTO {
    private String username;
    private String shopName;
    private String shopOwner;
    private Date createTime;
    private String idCard;
    private int pageNum;
    private int size;
}
