package com.farmhouse.demo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.farmhouse.demo.entity.Type;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author 千山暮雪
 * @since 2020-04-03
 */
public interface ITypeService extends IService<Type> {

    IPage<Type> getByPage(Type type, IPage<Type> page);

    /**
     * @return ${return_type}
     * @description: 删除商品类别
     * @date 2020/4/8 23:35
     */
    int deleteType(String typeId);

    /**
     * @return ${return_type}
     * @description: 修改类别
     * @date 2020/4/8 23:36
     */
    int updateType(Type type);

    /**
     * @description: 获取商品类别list
     * @date 2020/4/12 22:38
     * @return
     */
    List<Map<String, Object>> getTypeList(String shopId);
}
