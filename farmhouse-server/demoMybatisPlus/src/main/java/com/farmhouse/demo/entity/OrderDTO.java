package com.farmhouse.demo.entity;

import lombok.Data;

/**
 * @author 千山暮雪
 * @title:
 * @projectName:farmhouse-server
 * @description:
 * @date 2020/04/26/11:15
 */
@Data
public class OrderDTO {
    //订单编号
    public String orderCode;
    //分页的页数
    public int pageNum;
    //每页的最大条数
    public int size;
}
