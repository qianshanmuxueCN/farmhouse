package com.farmhouse.demo.entity;

import lombok.Data;

/**
 * @author 千山暮雪
 * @title: ImagesDTO
 * @projectName harmhouse
 * @description: 用于图片列表分页查询
 * @date 2020/4/14 17:21
 */
@Data
public class ImagesDTO {
    /**
     * 图片名称
     */
    private String imageName;
    /**
     * 图片类别 图片类别：1.商品图片，2.农家院图片
     */
    private Integer imageType;

    private int pageNum;
    private int size;
    //var params = {}
}
