package com.farmhouse.demo.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.util.ObjectUtils;

/**
 * <p>
 *
 * </p>
 *
 * @author 千山暮雪
 * @since 2020-04-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Images implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId(type = IdType.UUID)
    private String imageId;

    /**
     * 组ID
     */
    private String groupId;

    /**
     * 图片名称
     */
    private String imageName;

    /**
     * 图片路径
     */
    private String imageUrl;

    /**
     * 图片顺序
     */
    private Integer imageOrder;

    /**
     * 图片类别 图片类别：1.商品图片，2.农家院图片
     */
    private Integer imageType;

    /**
     * 图片归属商家
     */
    private String shopId;

    public Images(String imageId, String groupId, String imageName, String imageUrl, Integer imageOrder, Integer imageType, String shopId) {
        this.imageId = imageId;
        this.groupId = groupId;
        this.imageName = imageName;
        this.imageUrl = imageUrl;
        this.imageOrder = imageOrder;
        this.imageType = imageType;
        this.shopId = shopId;
    }

    public Images() {
    }

    public Images(ImagesDTO imagesDTO) {
        this.imageName = ObjectUtils.isEmpty(imagesDTO) ? null : imagesDTO.getImageName();
        this.imageType = ObjectUtils.isEmpty(imagesDTO) ? null : imagesDTO.getImageType();
    }

    /**
     * @return String
     * @description: 获取请求路径 但不是真实存在数据库，只是为了前端渲染
     * @date 2020/4/14 17:51
     */
    public String requestUrl() {
        return this.imageUrl + this.imageName;
    }
}
