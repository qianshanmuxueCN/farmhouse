package com.farmhouse.demo.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.util.StringUtils;

/**
 * <p>
 *
 * </p>
 *
 * @author 千山暮雪
 * @since 2020-04-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Commodity implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId(type = IdType.UUID)
    private String id;

    /**
     * 商家ID
     */
    private String shopId;

    /**
     * 商品标题
     */
    private String commodityTitle;

    /**
     * 商品类别ID
     */
    private String commodityTypeId;

    /**
     * 图片组ID
     */
    private String imagesGroupId;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    /**
     * 是否下架：0否，1是
     */
    private Integer isShelves;

    private BigDecimal price;//高精度数据计算

    /**
     * 库存
     */
    private Integer inventory;

    /**
     * 销量
     */
    @TableField("salesCount")
    private Integer salesCount;

    /**
     * 描述
     */
    private String description;

    /**
     * 是否打折 0否，1是
     */
    private Integer isDiscount;

    /**
     * 折扣价格
     */
    private BigDecimal discountPrice;

    /*VO*/
    @TableField(exist = false)
    private String typeName;
    /*VO*/
    @TableField(exist = false)
    private String imageUrl;

    public Commodity() {

    }

    public Commodity(CommodityDTO commodityDTO) {
        this.commodityTitle = StringUtils.isEmpty(commodityDTO.getCommodityTitle()) ? null : commodityDTO.getCommodityTitle();
        this.commodityTypeId = StringUtils.isEmpty(commodityDTO.getCommodityTypeId()) ? null : commodityDTO.getCommodityTypeId();
        this.isDiscount = StringUtils.isEmpty(commodityDTO.getIsDiscount()) ? null : commodityDTO.getIsDiscount();
    }
}
