package com.farmhouse.demo.service;

import com.farmhouse.demo.entity.Cart;
import com.baomidou.mybatisplus.extension.service.IService;
import com.farmhouse.demo.entity.CartVO;

import java.util.List;

/**
 * <p>
 * 购物车 服务类
 * </p>
 *
 * @author 千山暮雪
 * @since 2020-04-14
 */
public interface ICartService extends IService<Cart> {
    /**
     * @return ${return_type}
     * @description: 加入购物车
     * @date 2020/5/12 14:42
     */
    boolean addCart(Cart cart);

    /**
     * @param userId 用户ID
     * @return IPage
     * @description: 查询购物车列表
     * @date 2020/5/12 15:21
     */
    List<CartVO> selectListByUser(String userId);

    /**
     * @description: 删除购物车
     * @date 2020/5/12 21:14
     */
    boolean deleteCart(String userId, String cartId);

    /**
     * @description: 购物车结束
     * @date 2020/5/13 1:36
     */
    boolean finishCart(String userId,String[] cartIds);
}
