package com.farmhouse.demo.entity;

import lombok.Data;


/**
 * @author 千山暮雪
 * @title: OrderFromCartDTO
 * @projectName demo
 * @description: TODO
 * @date 2020/5/12 22:51
 */
@Data
public class OrderFromCartDTO {
    private static final long serialVersionUID = 1L;

    private String id;
    private Integer number;
    private Double price;
}
