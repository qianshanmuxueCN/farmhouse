package com.farmhouse.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.farmhouse.demo.dao.TypeMapper;
import com.farmhouse.demo.entity.Type;
import com.farmhouse.demo.service.ITypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 千山暮雪
 * @since 2020-04-03
 */
@Service
public class TypeServiceImpl extends ServiceImpl<TypeMapper, Type> implements ITypeService {

    @Resource
    private TypeMapper typeMapper;

    @Override
    public IPage<Type> getByPage(Type type, IPage<Type> page) {
        QueryWrapper<Type> typeQueryWrapper = new QueryWrapper<>();
        typeQueryWrapper.eq("shop_id", type.getShopId());
        if (!StringUtils.isEmpty(type.getTypeTitle()))
            typeQueryWrapper.like("type_title", type.getTypeTitle());
        return typeMapper.selectPage(page, typeQueryWrapper);
    }

    @Override
    public int deleteType(String typeId) {
        return typeMapper.deleteById(typeId);
    }

    @Override
    public int updateType(Type type) {
        UpdateWrapper<Type> updateWrapper = new UpdateWrapper<Type>();
        updateWrapper.set("type_title", type.getTypeTitle());
        updateWrapper.set("type_description", type.getTypeDescription());
        updateWrapper.eq("type_id", type.getTypeId());
        return typeMapper.update(type, updateWrapper);
    }

    @Override
    public List<Map<String, Object>> getTypeList(String shopId) {
        QueryWrapper<Type> typeQueryWrapper = new QueryWrapper<>();
        typeQueryWrapper.select("type_Id","type_title").eq("shop_id", shopId);
        return typeMapper.selectMaps(typeQueryWrapper);
    }
}
