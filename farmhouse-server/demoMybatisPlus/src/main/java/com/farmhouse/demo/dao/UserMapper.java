package com.farmhouse.demo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.farmhouse.demo.entity.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<User> {
}
