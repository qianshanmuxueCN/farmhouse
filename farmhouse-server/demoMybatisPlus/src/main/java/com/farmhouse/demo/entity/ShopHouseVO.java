package com.farmhouse.demo.entity;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 千山暮雪
 * @title: ShopHousVO
 * @projectName demo
 * @description: TODO
 * @date 2020/5/10 0:57
 */
@Data
@Accessors(chain = true)
public class ShopHouseVO {
    //商家属性
    private String id;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date createTime;
    private String username;
    private String shopName;
    private String address;
    private String phone;
    private String phone2;
    private Integer shopStatus;//店铺状态：0.打烊，1.营业中
    private String notice;//店铺公告
    //商品属性

    private String commodityTitle;//商品标题
    private BigDecimal price;//价格
    private Integer salesCount;//销量
    private String description;//描述
    private Integer isDiscount;//是否打折 0否，1是
    private BigDecimal discountPrice;//折扣价格
    //图片属性
    private String imageUrl;
}
