package com.farmhouse.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @author zhangxinyu
 * @title: WebMvcConfigurer
 * @projectName demo
 * @description: TODO
 * @date 2020/4/9 0:34
 */

@Configuration
public class WebMvcConfigurer  extends WebMvcConfigurerAdapter {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //http://localhost:8081/farmhouse/upload/image/01f8c7f7-4aef-42e1-903a-c983660b3c11.png
        registry.addResourceHandler("/upload/image/**").addResourceLocations("file:/opt/upload/images/");
    }
}
