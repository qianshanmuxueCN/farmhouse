package com.farmhouse.demo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.farmhouse.demo.entity.Admin;
import org.apache.ibatis.annotations.Mapper;


/*
* 管理员的dao
* @mapper mybatis的组件
* BaseMapper 接口提供了SQL语句的生成操作
* */
@Mapper
public interface AdminMapper extends BaseMapper<Admin> {

}
