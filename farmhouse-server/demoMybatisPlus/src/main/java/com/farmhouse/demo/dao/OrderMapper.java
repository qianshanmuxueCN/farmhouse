package com.farmhouse.demo.dao;

import com.farmhouse.demo.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author 千山暮雪
 * @since 2020-04-14
 */
@Mapper
public interface OrderMapper extends BaseMapper<Order> {

}
