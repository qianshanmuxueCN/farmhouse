package com.farmhouse.demo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.farmhouse.demo.entity.CartBO;
import com.farmhouse.demo.entity.Commodity;
import com.farmhouse.demo.entity.ShopHouseVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author 千山暮雪
 * @since 2020-04-03
 */
@Mapper
public interface CommodityMapper extends BaseMapper<Commodity> {
    /**
     * @return Page
     * @description: 分页查询农家大院商品信息
     * @date 2020/5/11 15:49
     */

    Page<ShopHouseVO> getShopHousePage(IPage<ShopHouseVO> page, String shopName);

    /**
     * @description: 店铺主页商品列表分页查询
     * @date 2020/5/11 23:36
     */
    Page<Commodity> getCommodityByShop(IPage<Commodity> page, String shopId, String typeId);

    /**
     * @description: 根据购物车ids查询商品数据
     * @date 2020/5/13 0:12
     */
    List<CartBO> getCommodityByCart(@Param("cartIds") String[] cartIds);
}
