package com.farmhouse.demo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import lombok.experimental.Accessors;
import org.springframework.util.ObjectUtils;

import java.util.Date;

/**
 * @author 千山暮雪
 * @title: ShopEntity
 * @projectName farmhouse-server
 * @description: 店铺实体类
 * @date 2020/3/30 21:37
 */
@Data
@Accessors(chain = true)
@TableName("shop")
public class ShopEntity {
    @TableId(type = IdType.UUID)
    private String id;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date createTime;
    private String updateUser;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date updateTime;
    private String username;
    private String password;
    private String shopName;
    private String shopOwner;
    private String idCard;
    private String address;
    private String phone;
    private String phone2;
    private Integer state;//店铺审核状态 0.待审核，1.审核未通过，2.审核通过
    private Integer shopStatus;//店铺状态：0.打烊，1.营业中
    private String notice;//店铺公告

    @TableField(exist = false)
    protected String updateUserName;

    public ShopEntity(ShopEntityDTO shopEntityDTO) {
        this.username = ObjectUtils.isEmpty(shopEntityDTO.getUsername()) ? null : shopEntityDTO.getUsername().trim();
        this.shopName = ObjectUtils.isEmpty(shopEntityDTO.getShopName()) ? null : shopEntityDTO.getShopName().trim();
        this.shopOwner = ObjectUtils.isEmpty(shopEntityDTO.getShopOwner()) ? null : shopEntityDTO.getShopOwner().trim();
        this.idCard = ObjectUtils.isEmpty(shopEntityDTO.getIdCard()) ? null : shopEntityDTO.getIdCard().trim();
    }

    public ShopEntity() {
    }

    public ShopEntity(String id, Date createTime, String updateUser, Date updateTime, String username, String password, String shopName, String shopOwner, String idCard, String address, String phone, String phone2, Integer state, Integer shopStatus, String notice, String updateUserName) {
        this.id = id;
        this.createTime = createTime;
        this.updateUser = updateUser;
        this.updateTime = updateTime;
        this.username = username;
        this.password = password;
        this.shopName = shopName;
        this.shopOwner = shopOwner;
        this.idCard = idCard;
        this.address = address;
        this.phone = phone;
        this.phone2 = phone2;
        this.state = state;
        this.shopStatus = shopStatus;
        this.notice = notice;
        this.updateUserName = updateUserName;
    }
}
