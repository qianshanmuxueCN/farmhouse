package com.farmhouse.demo.entity;

import lombok.Data;

/**
 * @author 千山暮雪
 * @title: ShopHouseDTO
 * @projectName demo
 * @description: 首页获取商家大院
 * @date 2020/5/11 17:40
 */
@Data
public class ShopHouseDTO {
    private String shopName;
    private int pageNum;
    private int size;
}
