package com.farmhouse.demo.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.farmhouse.demo.entity.*;
import com.farmhouse.demo.service.ICartService;
import com.farmhouse.demo.service.IChildOrderService;
import com.farmhouse.demo.service.ICommodityService;
import com.farmhouse.demo.service.IOrderService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.ibatis.transaction.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;

import static org.springframework.transaction.annotation.Isolation.READ_COMMITTED;
import static org.springframework.transaction.annotation.Isolation.SERIALIZABLE;
import static org.springframework.transaction.annotation.Propagation.REQUIRED;

/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author 千山暮雪
 * @since 2020-04-14
 */
@Api(value = "订单接口")
@RestController
@RequestMapping("/order/order")
@Slf4j
public class OrderController {
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private IOrderService orderService;
    @Autowired
    private ICommodityService commodityService;
    @Autowired
    private IChildOrderService childOrderService;
    @Autowired
    private ICartService cartService;


    @GetMapping("/getOrderList")
    public CommonResult<IPage<Order>> getByWrapper(OrderDTO orderDTO) {
        String shopId = (String) request.getSession().getAttribute("userId");
        if (StringUtils.isEmpty(shopId))
            return new CommonResult<>(403, "请重新登录");
        IPage<Order> page = new Page<>();
        page.setCurrent(orderDTO.getPageNum());
        page.setSize(orderDTO.getSize());
        IPage<Order> orderIPage = orderService.getByWrapper(orderDTO.getOrderCode(), shopId, page);
        return ObjectUtils.isEmpty(orderIPage) && ObjectUtils.isEmpty(orderIPage.getRecords()) ?
                new CommonResult<>(304, "查询失败") :
                new CommonResult<IPage<Order>>(200, "查询成功", orderIPage);
    }

    /**
     * @return ${return_type}
     * @description: 用户查询订单
     * @date 2020/5/14 1:49
     */
    @GetMapping("/getOrderListByUser")
    public CommonResult<IPage<Order>> getOrderListByUser(Integer pageNum, int size) {
        String userId = (String) request.getSession().getAttribute("userId");
        if (StringUtils.isEmpty(userId))
            return new CommonResult<>(403, "请重新登录");
        IPage<Order> page = new Page<>();
        page.setCurrent(pageNum);
        page.setSize(size);
        IPage<Order> orderIPage = orderService.getOrderListByUser(userId, page);
        return ObjectUtils.isEmpty(orderIPage) && ObjectUtils.isEmpty(orderIPage.getRecords()) ?
                new CommonResult<>(304, "查询失败") :
                new CommonResult<IPage<Order>>(200, "查询成功", orderIPage);
    }

    /**
     * @return ${return_type}
     * @description: 结算商品
     * @date 2020/5/12 23:46
     * isolation = Isolation.SERIALIZABLE 串行化
     * propagation=Propagation.REQUIRES_NEW 不管是否存在事务,都创建一个新的事务,原来的挂起,新的执行完毕,继续执行老的事务
     * rollbackFor=Exception.class 发生异常回滚提交
     */
    @PostMapping("/payOrder")
    @Transactional(readOnly = false, isolation = READ_COMMITTED, propagation = REQUIRED, rollbackFor = Exception.class)
    public CommonResult payOrder(@RequestBody OrderFromCartDTO[] orderFromCartDTOS) {
        //上传参数校验
        String userId = (String) request.getSession().getAttribute("userId");
        if (StringUtils.isEmpty(userId))
            return new CommonResult(403, "请重新登录");
        if (orderFromCartDTOS == null || orderFromCartDTOS.length == 0)
            return new CommonResult(417, "请勾选购物车商品");
        Map<String, Integer> numMap = new HashMap<>();
        Map<String, Double> priceMap = new HashMap<>();
        String[] cartIds = new String[orderFromCartDTOS.length];//用于查询是否都在该用户的购物车中
        for (int i = 0; i < orderFromCartDTOS.length; i++) {
            cartIds[i] = orderFromCartDTOS[i].getId();
            numMap.put(orderFromCartDTOS[i].getId(), orderFromCartDTOS[i].getNumber());
            priceMap.put(orderFromCartDTOS[i].getId(), orderFromCartDTOS[i].getPrice());
        }

        // 1.查询该用户的购物车是否存在此商品；并且查询商品库存数量是否足够；是否下架，价格是否被串改
        List<CartBO> cartBOList = commodityService.getCommodityByCart(cartIds);
        if (cartBOList == null || cartBOList.size() == 0 || cartIds.length != cartBOList.size())
            return new CommonResult(417, "请重新勾选购物车商品");
        String timeCode = DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS");
        //生成订单编号
        String orderCode = timeCode + UUID.randomUUID().toString().replaceAll("-", "").substring(8, 12);
        //创建父订单对象
        Order order = new Order().setCreateTime(new Date()).setOrderCode(orderCode);
        //创建子订单对象
        List<ChildOrder> childOrderList = new ArrayList<>();
        Integer comSum = 0;
        Double moneySum = 0D;
        for (CartBO cartBo : cartBOList) {
            ChildOrder childOrder = new ChildOrder().setCreateTime(new Date()).setOrderCode(orderCode);
            //是否下架
            if (cartBo.getIsShelves() == 1) {
                return new CommonResult(417, "该商品已下架");
            }
            //数量是否足够
            if (cartBo.getInventory() < numMap.get(cartBo.getCartId())) {
                return new CommonResult(417, "该商品库存数量不足");
            }
            //价格是否被串改
            if (!cartBo.getPrice().equals(priceMap.get(cartBo.getCartId()))) {
                if (!cartBo.getDiscountPrice().equals(priceMap.get(cartBo.getCartId())))
                    return new CommonResult(417, "该商品价格已更新");
            }
            //累加商品总数
            comSum += numMap.get(cartBo.getCartId());
            //累加订单商品总价
            moneySum += numMap.get(cartBo.getCartId()) * priceMap.get(cartBo.getCartId());
            //完善子订单数据
            childOrder.setCommodityId(cartBo.getCommodityId())//设置商品id
                    .setCommodityNumber(numMap.get(cartBo.getCartId()))//设置商品数量
                    .setCommodityPrice(new BigDecimal(cartBo.getPrice()));//设置原价
            if (cartBo.getIsDiscount() == 1)
                childOrder.setTransactionPrice(new BigDecimal(cartBo.getDiscountPrice()));
            else
                childOrder.setTransactionPrice(new BigDecimal(cartBo.getPrice()));
            childOrderList.add(childOrder);
        }
        // 2.创建订单，价格根据数据库中查询的商品价格进行计算；如果有打折，按照折扣价格进行计算
        //完善父订单的数据
        order.setUserId(userId)
                .setCommodityNumber(comSum)
                .setTotalMoney(new BigDecimal(moneySum))
                .setOrderState(0);

        //保存子订单，父订单的数据
        for (ChildOrder c : childOrderList) {
            childOrderService.save(c);
        }
        orderService.save(order);
        // 3.修改商品库存        /*销量的修改放到订单支付步骤中*/
        for (CartBO cartBO : cartBOList) {
            commodityService.updateInventory(cartBO.getCommodityId(), cartBO.getInventory() - numMap.get(cartBO.getCartId()));
        }
        // 4.修改购物车数据
        cartService.finishCart(userId, cartIds);

        //dataSourceTransactionManager.commit(transactionStatus);//提交
        return new CommonResult(200, "订单创建成功，请前往我的订单支付");
    }

    /**
     * @return CommonResult
     * @description: 支付订单
     * @date 2020/5/14 18:46
     */
    @PostMapping("/updateOrderState")
    public CommonResult updateOrderState(@Validated EditOrderDTO editOrderDTO) {
        String userId = (String) request.getSession().getAttribute("userId");
        if (StringUtils.isEmpty(userId) || !userId.equals(editOrderDTO.getUserId()))
            return new CommonResult(403, "请重新登录");
        else
            return new CommonResult<>(200, "", orderService.editOrderStatus(editOrderDTO));
    }

}
