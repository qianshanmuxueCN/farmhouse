package com.farmhouse.demo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.farmhouse.demo.entity.ShopEntity;

import java.util.List;
public interface ShopService extends IService<ShopEntity> {
    /**
     * @param username
     * @param password
     * @return ShopEntity
     * @description: 商家的登录方法
     * @date 2020/3/30 21:56
     */
    ShopEntity login(String username, String password);

    /**
     * @param shopName
     * @param username
     * @param password
     * @return ShopEntity
     * @description: 商家的注册方法
     * @date 2020/3/30 21:56
     */
    ShopEntity register(String shopName, String username, String password);

    /**
     * @param shopEntity
     * @return ShopEntity
     * @description: 商家完善商户信息
     * @date 2020/3/30 21:56
     */
    ShopEntity updateInfo(ShopEntity shopEntity);

    /**
     * @param id
     * @return ShopEntity
     * @description: 根据ID查询店铺商家的详细信息
     * @date 2020/3/30 23:08
     */
    ShopEntity getShopEntityById(String id);

    /**
     * @param shopEntity
     * @return List<ShopEntity>
     * @description: 根据条件分页查询店铺商家的信息列表
     * @date 2020/3/30 23:08
     */
    IPage<ShopEntity> getByWrapper(ShopEntity shopEntity, IPage page);

    /**
     * @param id
     * @param notice
     * @return int
     * @description: 根据ID修改商家的公告信息
     * @date 2020/3/31 0:18
     */
    int updateNotice(String id, String notice);

    /**
     * @param shopId
     * @param adminId
     * @param result
     * @return int
     * @description: 审核商家注册状态
     * @date 2020/4/1 21:47
     */
    int auditShopState(String shopId, String adminId, int result);

    /**
     * @param shopId
     * @param shopStatus
     * @return int
     * @description: 商家修改店铺状态
     * @date 2020/4/1 22:04
     */
    int updateShopStatusByShopOwner(String shopId, int shopStatus);

    /**
     * @param shopId
     * @return int
     * @description: 管理员重置商家密码
     * @date 2020/4/1 22:19
     */
    int resetPasswordByAdmin(String shopId);

    /**
     * @param shopId
     * @param oldPassword
     * @param newPassword
     * @return int
     * @description: 商家自己修改密码
     * @date 2020/4/1 22:20
     */
    int updatePassword(String shopId, String oldPassword, String newPassword);
}
