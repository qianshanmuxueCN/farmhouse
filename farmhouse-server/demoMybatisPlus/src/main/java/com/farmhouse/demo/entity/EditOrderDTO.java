package com.farmhouse.demo.entity;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author ZhangXinYu
 * @title: EditOrderDTO
 * @projectName demo
 * @description: 用于修改订单，支付订单，DTO
 * @date 2020/5/14 18:48
 */
@Data
public class EditOrderDTO {
    @NotEmpty
    private String userId;
    @NotEmpty
    private String orderId;
    @NotNull
    private Integer orderState;
}
