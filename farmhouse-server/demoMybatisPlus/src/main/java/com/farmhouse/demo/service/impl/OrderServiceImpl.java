package com.farmhouse.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.farmhouse.demo.entity.EditOrderDTO;
import com.farmhouse.demo.entity.Order;
import com.farmhouse.demo.dao.OrderMapper;
import com.farmhouse.demo.service.IOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.Date;


/**
 * <p>
 * 订单表 服务实现类
 * </p>
 *
 * @author 千山暮雪
 * @since 2020-04-14
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements IOrderService {
    @Resource
    public OrderMapper orderDao;

    @Override

    public IPage<Order> getByWrapper(String orderCode,String shopId, IPage<Order> page) {
        QueryWrapper<Order> orderQueryWrapper = new QueryWrapper<>();
        orderQueryWrapper.eq("shop_Id",shopId).eq("order_code",orderCode);
        IPage<Order> iPage=orderDao.selectPage(page,orderQueryWrapper);
        return iPage;
    }

    @Override
    public IPage<Order> getOrderListByUser(String userId, IPage<Order> page) {
        QueryWrapper<Order> orderQueryWrapper = new QueryWrapper<>();
        orderQueryWrapper.eq("user_id",userId);
        IPage<Order> iPage=orderDao.selectPage(page,orderQueryWrapper);
        return iPage;
    }

    @Override
    public boolean editOrderStatus(EditOrderDTO editOrderDTO) {
        UpdateWrapper<Order> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("order_state",editOrderDTO.getOrderState())
                .set("payment_time",new Date())
                .eq("user_id", editOrderDTO.getUserId())
                .eq("order_id", editOrderDTO.getOrderId());

        return update(updateWrapper);
    }
}
