package com.farmhouse.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.farmhouse.demo.dao.CommodityMapper;
import com.farmhouse.demo.dao.TypeMapper;
import com.farmhouse.demo.entity.CartBO;
import com.farmhouse.demo.entity.Commodity;
import com.farmhouse.demo.entity.ShopHouseVO;
import com.farmhouse.demo.entity.Type;
import com.farmhouse.demo.service.ICommodityService;
import com.farmhouse.demo.service.ITypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 千山暮雪
 * @since 2020-04-03
 */
@Service
public class CommodityServiceImpl extends ServiceImpl<CommodityMapper, Commodity> implements ICommodityService {
    @Resource
    private CommodityMapper commodityMapper;
    @Resource
    private TypeMapper typeMapper;

    @Override
    public IPage<Commodity> getByPage(Commodity commodity, IPage<Commodity> page, String shopId) {
        QueryWrapper<Commodity> commodityQueryWrapper = new QueryWrapper<>(commodity);
        commodityQueryWrapper.eq("shop_id", shopId);
        if (!StringUtils.isEmpty(commodity.getCommodityTitle())) {
            commodityQueryWrapper.eq("commodity_title", commodity.getCommodityTitle());
        }
        if (!StringUtils.isEmpty(commodity.getIsDiscount())) {
            commodityQueryWrapper.eq("is_discount", commodity.getIsDiscount());
        }
        if (!StringUtils.isEmpty(commodity.getCommodityTypeId())) {
            commodityQueryWrapper.eq("commodity_type_id", commodity.getCommodityTypeId());
        }
        IPage<Commodity> iPage = commodityMapper.selectPage(page, commodityQueryWrapper);
        QueryWrapper<Type> typeQueryWrapper = new QueryWrapper<>();
        typeQueryWrapper.select("type_Id", "type_title").eq("shop_id", shopId);
        List<Map<String, Object>> list = typeMapper.selectMaps(typeQueryWrapper);
        Map<String, String> typeMap = new HashMap<>();
        for (Map<String, Object> map : list) {
            typeMap.put((String) map.get("type_Id"), (String) map.get("type_title"));
        }
        for (Commodity c : iPage.getRecords()) {
            c.setTypeName(typeMap.get(c.getCommodityTypeId()));
        }
        return iPage;
    }

    @Override
    public void deleteById(String id) {
        commodityMapper.deleteById(id);
    }

    @Override
    public boolean discount(String id, int isDiscount, Double price) {
        UpdateWrapper<Commodity> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("discount_price", price);
        updateWrapper.set("is_discount", isDiscount);
        updateWrapper.eq("id", id);
        return update(updateWrapper);
    }

    @Override
    public boolean shelves(String commodityId, Integer isShelves) {
        UpdateWrapper<Commodity> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("is_shelves", isShelves);
        updateWrapper.eq("id", commodityId);
        return update(updateWrapper);
    }

    @Override
    public boolean inventory(String commodityId, int inventory) {
        UpdateWrapper<Commodity> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("inventory", inventory);
        updateWrapper.eq("id", commodityId);
        return update(updateWrapper);
    }

    @Override
    public boolean update(Commodity commodity) {
        UpdateWrapper<Commodity> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("commodity_title", commodity.getCommodityTitle());
        updateWrapper.set("commodity_type_id", commodity.getCommodityTypeId());
        updateWrapper.set("inventory", commodity.getInventory());
        updateWrapper.set("price", commodity.getPrice());
        updateWrapper.eq("id", commodity.getId());
        return update(updateWrapper);
    }

    @Override
    public Page<ShopHouseVO> getShopHousePage(IPage<ShopHouseVO> page, String shopName) {
        return commodityMapper.getShopHousePage(page, shopName);
    }

    @Override
    public Page<Commodity> getCommodityByShop(IPage<Commodity> page, String shopId, String typeId) {
        return commodityMapper.getCommodityByShop(page, shopId, typeId);
    }

    @Override
    public List<CartBO> getCommodityByCart(String[] cartIds) {
        return commodityMapper.getCommodityByCart(cartIds);
    }

    @Override
    public boolean updateInventory(String commodityId, Integer number) {
        UpdateWrapper<Commodity> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("inventory", number).eq("id", commodityId);
        return update(updateWrapper);
    }

    @Override
    public List<Commodity> getListByShop(String shopId) {
        QueryWrapper<Commodity> commodityQueryWrapper = new QueryWrapper<>();
        commodityQueryWrapper.eq("shop_id", shopId);
        return commodityMapper.selectList(commodityQueryWrapper);
    }
}
