package com.farmhouse.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.farmhouse.demo.dao.CommodityMapper;
import com.farmhouse.demo.dao.ShopMapper;
import com.farmhouse.demo.entity.Commodity;
import com.farmhouse.demo.entity.ShopEntity;
import com.farmhouse.demo.service.ShopService;
import com.farmhouse.demo.utils.MD5Util;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;

import static org.apache.tomcat.jni.SSL.setPassword;

/**
 * @author 千山暮雪
 * @title: ShopServiceImpl
 * @projectName farmhouse-server
 * @description: 商家服务实现
 * @date 2020/3/30 21:48
 */
@Service
public class ShopServiceImpl extends ServiceImpl<ShopMapper, ShopEntity> implements ShopService {
    @Resource
    private ShopMapper shopDao;
    @Resource
    private CommodityMapper commodityMapper;

    @Override
    public ShopEntity login(String username, String password) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("username", username);
        ShopEntity one = getOne(queryWrapper);
        if (!ObjectUtils.isEmpty(one) && MD5Util.checkpassword(password, one.getPassword())) {
            one.setPassword("");
            return one;
        } else {
            return null;
        }
    }

    @Override
    public ShopEntity register(String shopName, String username, String password) {
        //构建查询条件
        QueryWrapper queryWrapper = new QueryWrapper<ShopEntity>();
        queryWrapper.eq("username", username);
        queryWrapper.or();
        queryWrapper.eq("shop_name", shopName);
        //根据查询条件查询数据库数据的条数
        Integer count = shopDao.selectCount(queryWrapper);
        //如果条数==0 说明数据库不存在用户想要注册的用户名和店铺名
        if (count == 0) {
            ShopEntity shopEntity = new ShopEntity()
                    .setShopName(shopName)
                    .setUsername(username)
                    .setPassword(password)
                    .setCreateTime(new Date());
            int c = shopDao.insert(shopEntity);
            //自动为每个注册的商家添加农家院基础商品
            ShopEntity one = getOne(queryWrapper);
            if(c==1){
                Commodity commodity =new Commodity();
                commodity.setCommodityTypeId("1");//属于大院商品
                commodity.setCreateTime(new Date())
                        .setIsDiscount(0)
                        .setCommodityTitle("农家大院")
                        .setDescription("大院商品，默认创建，请商家及时修改")
                        .setPrice(new BigDecimal(0D))
                        .setShopId(one.getId());
                commodityMapper.insert(commodity);
            }
            return c == 1 ? one.setPassword("") : null;
        } else {
            //账户名已存在
            return null;
        }
    }

    @Override
    public ShopEntity updateInfo(ShopEntity shopEntity) {
        ShopEntity byId = getById(shopEntity.getId());
        byId.setShopOwner(shopEntity.getShopOwner())
                .setIdCard(shopEntity.getIdCard())
                .setAddress(shopEntity.getAddress())
                .setPhone(shopEntity.getPhone())
                .setPhone2(shopEntity.getPhone2());
        int i = shopDao.updateById(shopEntity.setUpdateTime(new Date()));
        return i == 1 ? shopEntity : null;
    }

    @Override
    public ShopEntity getShopEntityById(String id) {
        ShopEntity byId = getById(id);
        return ObjectUtils.isEmpty(byId) ? null : byId.setPassword("");
    }

    @Override
    public IPage<ShopEntity> getByWrapper(ShopEntity shopEntity, IPage page) {
        IPage<ShopEntity> iPage = shopDao.selectPage(page, new QueryWrapper<ShopEntity>(shopEntity));
        for (ShopEntity s : iPage.getRecords()) {
            s.setPassword("");
        }
        return iPage;
    }

    @Override
    public int updateNotice(String id, String notice) {
        ShopEntity byId = getById(id);
        int i = shopDao.updateById(byId.setNotice(notice));
        return i;
    }

    @Override
    public int auditShopState(String shopId, String adminId, int result) {
        ShopEntity byId = getById(shopId);
        int i = shopDao.updateById(byId.setState(result).setUpdateUser(adminId).setUpdateTime(new Date()));
        return i;
    }

    @Override
    public int updateShopStatusByShopOwner(String shopId, int shopStatus) {
        ShopEntity byId = getById(shopId);
        int i = shopDao.updateById(byId.setShopStatus(shopStatus));
        return i;
    }

    @Override
    public int resetPasswordByAdmin(String shopId) {
        ShopEntity byId = getById(shopId);
        if (ObjectUtils.isEmpty(byId))
            return 0;
        int i = shopDao.updateById(byId.setPassword(MD5Util.encoderByMd5("123456")).setUpdateTime(new Date()));
        return i;
    }

    @Override
    public int updatePassword(String shopId, String oldPassword, String newPassword) {
        ShopEntity byId = getById(shopId);
        int i = 0;
        //判断旧密码是否相同
        if (MD5Util.checkpassword(oldPassword, byId.getPassword())) {
            i = shopDao.updateById(byId.setPassword(MD5Util.encoderByMd5(newPassword)));
        }
        return i;
    }
}
