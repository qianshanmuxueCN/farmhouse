package com.farmhouse.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.farmhouse.demo.dao.UserMapper;
import com.farmhouse.demo.entity.User;
import com.farmhouse.demo.entity.UserDTO;
import com.farmhouse.demo.service.UserService;
import com.farmhouse.demo.utils.MD5Util;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.Date;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    @Resource
    private UserMapper userDao;

    /***
     * 用户登陆
     * @param username
     * @param password
     * @return
     */
    @Override
    public User login(String username, String password) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("username", username);
        User one = getOne(queryWrapper);
        if (!ObjectUtils.isEmpty(one) && MD5Util.checkpassword(password, one.getPassword())) {
            one.setPassword("");
            return one;
        } else {
            return null;
        }
    }

    /***
     * 用户注册
     * @param username
     * @param password
     * @param phone
     * @param gender
     * @param userEmail
     * @param age
     * @return
     */
    @Override
    public User register(String username, String password, String phone, int gender, String userEmail, int age) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("username", username);
        //查找列数
        Integer count = userDao.selectCount(queryWrapper);
        if (count == 0) {
            User user = new User();
            user.setUsername(username);
            user.setPassword(MD5Util.encoderByMd5(password));
            user.setAge(age);
            user.setPhone(phone);
            user.setUserEmail(userEmail);
            user.setGender(gender);
            user.setCreateTime(new Date());
            int c = userDao.insert(user);
            getOne(queryWrapper).setPassword("");
            return c == 1 ? getOne(queryWrapper) : null;
        } else {
            return null;
        }
    }

    /***
     * 根据id修改用户基本信息
     * @param user
     * @return
     */
    @Override
    public User updateInfo(User user) {
        user.setUsername(user.getUsername());
        user.setGender(user.getGender());
        user.setAge(user.getAge());
        user.setUserEmail(user.getUserEmail());
        user.setPhone(user.getPhone());
        int i = userDao.updateById(user);
        return !ObjectUtils.isEmpty(i) ? user : null;
    }

    /***
     * 根据id获取用户全部信息
     * @param id
     * @return
     */
    @Override
    public User getUserById(String id) {
        User i = getById(id);
        return ObjectUtils.isEmpty(i) ? null : i;
    }

    /***
     * 用户修改密码
     * @param UserId
     * @param oldPassword
     * @param newPassword
     * @return
     */
    @Override
    public int updatePassword(String UserId, String oldPassword, String newPassword) {
        User byId = getById(UserId);
        int i = 0;
        //判断旧密码是否相同
        if (MD5Util.checkpassword(oldPassword, byId.getPassword())) {
            i = userDao.updateById(byId.setPassword(MD5Util.encoderByMd5(newPassword)));
        }
        return i;
    }

    /***
     * 用户重置密码
     * @param username
     * @param phone
     * @param userEmail
     * @return
     */
    @Override
    public int resetPassword(String username, String phone, String userEmail) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username", username)
                    .eq("phone", phone)
                    .eq("username", username);
        User one = getOne(queryWrapper);
        String userId=one.getId();
        if(!ObjectUtils.isEmpty(userId)){
            int i=userDao.updateById(one.setPassword(MD5Util.encoderByMd5("123456")));
            return (i==1)?1:0;
        }
       return 0;
    }


    /***
     *
     * @param user
     * @param page
     * @return
     */
    @Override
    public IPage<User> getByWrapper(User user, IPage page) {
        IPage<User> iPage=userDao.selectPage(page,new QueryWrapper<User>(user));
        for(User s: iPage.getRecords() ){
            s.setPassword("");
        }
        return iPage;
    }

    @Override
    public int deleteById(String id) {
        int i=userDao.deleteById(id);
        return (i==1)?1:0;
    }

    @Override
    public int resetPasswordById(String id,String adminId) {
        User byId=getById(id);
        int i=userDao.updateById(byId.setPassword(MD5Util.encoderByMd5("123456")));
        return i;
    }
}
