package com.farmhouse.demo.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.util.ObjectUtils;

/**
 * <p>
 * 订单表
 * </p>
 *
 * @author 千山暮雪
 * @since 2020-04-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("Forder")
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 订单ID
     */
    @TableId
    private String orderId;

    /**
     * 订单编号
     */
    private String orderCode;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 商品数量
     */
    private Integer commodityNumber;

    /**
     * 订单money
     */
    private BigDecimal totalMoney;
    /**
     * 订单状态：0待付款，1已付款，2取消订单，3完成交易，4退款
     */
    private Integer orderState;

    /**
     * 支付时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date paymentTime;

    /**
     * 退款时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date refundTime;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    public Order(OrderDTO orderDTO) {
        this.orderCode = ObjectUtils.isEmpty(orderDTO.getOrderCode()) ? null : orderDTO.getOrderCode().trim();
    }

    public Order() {
    }
}
