package com.farmhouse.demo.entity;

/*
 * 管理员实体类
 * 实现Serializable接口时用于实现序列化的
 * */

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @author
 * @title: Admin
 * @projectName farmhouse-server
 * @description: 管理员实体类
 * @date 2020/3/23 21:38
 */
@Data
@Builder
@TableName("admin")
public class Admin implements Serializable {
    @TableId(type = IdType.UUID)
    private String id;
    private String username;
    private String password;
}
