package com.farmhouse.demo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.farmhouse.demo.entity.EditOrderDTO;
import com.farmhouse.demo.entity.Order;
import com.baomidou.mybatisplus.extension.service.IService;


/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author 千山暮雪
 * @since 2020-04-14
 */
public interface IOrderService extends IService<Order> {
    IPage<Order> getByWrapper(String orderCode, String shopId, IPage<Order> page);

    /**
     * @return ${return_type}
     * @description: 用户查询订单
     * @date 2020/5/14 1:49
     */
    IPage<Order> getOrderListByUser(String userId, IPage<Order> page);


    /**
     * @description: 根据订单ID和用户ID修改订单状态
     * @date 2020/5/14 18:58
     */
    boolean editOrderStatus(EditOrderDTO editOrderDTO);
}
