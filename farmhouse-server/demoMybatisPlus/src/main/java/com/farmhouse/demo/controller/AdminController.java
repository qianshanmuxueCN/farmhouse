package com.farmhouse.demo.controller;

import com.farmhouse.demo.entity.Admin;
import com.farmhouse.demo.entity.CommonResult;
import com.farmhouse.demo.service.AdminService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 千山暮雪
 * @title: AdminController
 * @projectName farmhouse-server
 * @description: admin的接口类
 * @date 2020/3/23 21:51
 */
@Api(value = "管理员接口")
@RestController
@RequestMapping("/admin")
@Slf4j
public class AdminController {

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private AdminService adminService;

    /**
     * @param username
     * @param password
     * @return CommonResult
     * @description: 管理员登录方法
     * @date 2020/3/23 22:17
     */
    @PostMapping("/login")
    public CommonResult<Admin> login(String username, String password) {
        //数据校验
        if (!StringUtils.isEmpty(username) && !StringUtils.isEmpty(password)) {
            //调用服务
            Admin result = adminService.login(username, password);
            if (result != null) {
                request.getSession().setAttribute("userName", username);
                request.getSession().setAttribute("userId", result.getId());
            }
            //打印日志
            log.info("{}用户登录结果：{}", username, !ObjectUtils.isEmpty(result));
            //返回结果给前端
            return ObjectUtils.isEmpty(result) ? new CommonResult<Admin>(303, "用户名或者密码错误！") :
                    new CommonResult<Admin>(200, "登录成功！", result);
        } else {
            log.info("{}用户登录失败", username);
            return new CommonResult<Admin>(300, "参数有误！");
        }
    }

}

