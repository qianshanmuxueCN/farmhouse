package com.farmhouse.demo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.farmhouse.demo.entity.Type;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 千山暮雪
 * @since 2020-04-03
 */
@Mapper
public interface TypeMapper extends BaseMapper<Type> {

}
