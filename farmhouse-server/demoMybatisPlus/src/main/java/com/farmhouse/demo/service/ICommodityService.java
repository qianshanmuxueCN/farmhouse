package com.farmhouse.demo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.farmhouse.demo.entity.CartBO;
import com.farmhouse.demo.entity.Commodity;
import com.baomidou.mybatisplus.extension.service.IService;
import com.farmhouse.demo.entity.ShopHouseVO;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author 千山暮雪
 * @since 2020-04-03
 */
public interface ICommodityService extends IService<Commodity> {

    IPage<Commodity> getByPage(Commodity commodity, IPage<Commodity> page, String shopId);

    /**
     * @description: 根据ID删除
     * @date 2020/4/11 18:31
     */
    void deleteById(String commodityId);

    /**
     * @description: 商品打折
     * @date 2020/4/9 0:55
     */
    boolean discount(String id, int isDiscount, Double price);

    /**
     * @description: 商品上下架
     * @date 2020/4/11 18:01
     */
    boolean shelves(String commodityId, Integer isShelves);

    /**
     * @description: 修改库存
     * @date 2020/4/11 18:31
     */
    boolean inventory(String commodityId, int inventory);

    /***
     * 修改商品
     * @param commodity 商品数据
     *
     */
    boolean update(Commodity commodity);

    /**
     * @return Page
     * @description: 获取商家大院列表
     * @date 2020/5/11 17:34
     */
    Page<ShopHouseVO> getShopHousePage(IPage<ShopHouseVO> page, String shopName);


    /**
     * @description: 店铺主页商品列表分页查询
     * @date 2020/5/11 23:36
     */
    Page<Commodity> getCommodityByShop(IPage<Commodity> page, String shopId, String typeId);

    /**
     * @description: 根据购物车ids查询商品数据
     * @date 2020/5/13 0:12
     */
    List<CartBO> getCommodityByCart(String[] cartIds);

    /**
     * @description: 修改商品库存
     * @date 2020/5/13 1:46
     */
    boolean updateInventory(String commodityId, Integer number);

    /**
     * @description: 查询商品列表
     * @date 2020/5/14 20:42
     */
    List<Commodity> getListByShop(String shopId);
}
