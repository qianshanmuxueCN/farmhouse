package com.farmhouse.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.farmhouse.demo.entity.Cart;
import com.farmhouse.demo.dao.CartMapper;
import com.farmhouse.demo.entity.CartVO;
import com.farmhouse.demo.service.ICartService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 购物车 服务实现类
 * </p>
 *
 * @author 千山暮雪
 * @since 2020-04-14
 */
@Service
public class CartServiceImpl extends ServiceImpl<CartMapper, Cart> implements ICartService {

    @Resource
    private CartMapper cartMapper;

    @Override
    public boolean addCart(Cart cart) {
        QueryWrapper<Cart> queryWrapper = new QueryWrapper();
        queryWrapper.eq("user_id", cart.getUserId());
        queryWrapper.eq("cart_state",0);
        queryWrapper.eq("commodity_id", cart.getCommodityId());
        Integer integer = cartMapper.selectCount(queryWrapper);
        if (integer == 0)
            return save(cart);
        else
            return false;
    }

    @Override
    public List<CartVO> selectListByUser(String userId) {
        return cartMapper.selectListByUser(userId);
    }

    @Override
    public boolean deleteCart(String userId, String cartId) {
        UpdateWrapper<Cart> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("user_id", userId)
                .eq("cart_id", cartId);
        return cartMapper.delete(updateWrapper) > 0;
    }

    @Override
    public boolean finishCart(String userId,String[] cartIds) {
        return cartMapper.finishCart(userId,cartIds) == cartIds.length;
    }

}
