package com.farmhouse.demo.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.farmhouse.demo.entity.Cart;
import com.farmhouse.demo.entity.CartPageDTO;
import com.farmhouse.demo.entity.CartVO;
import com.farmhouse.demo.entity.CommonResult;
import com.farmhouse.demo.service.ICartService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 购物车 前端控制器
 * </p>
 *
 * @author 千山暮雪
 * @since 2020-04-14
 */
@Api(value = "购物车接口")
@RestController
@RequestMapping("/cart")
public class CartController {

    @Autowired
    private ICartService cartService;
    @Autowired
    private HttpServletRequest request;

    /**
     * @param cart obj
     * @description: 添加购物车
     * @date 2020/5/12 14:48
     */
    @PostMapping("/addCart")
    public CommonResult addCart(@Validated Cart cart) {
        String userId = (String) request.getSession().getAttribute("userId");
        if (StringUtils.isEmpty(userId) || cart == null || StringUtils.isEmpty(cart.getUserId()) || !userId.equals(cart.getUserId()))
            return new CommonResult(200, "参数有误");
        else {
            boolean b = cartService.addCart(cart.setCartState(0).setNumber(1));
            return b ? new CommonResult(200, "添加成功") : new CommonResult(200, "已添加");
        }
    }

    /**
     * @description: 分页查询购物车列表
     * @date 2020/5/12 15:13
     */
    @GetMapping("/getCartPage")
    public CommonResult getCartPage() {
        String userId = (String) request.getSession().getAttribute("userId");
        List<CartVO> cartPage = cartService.selectListByUser(userId);
        return new CommonResult<>(200, "", cartPage);
    }

    /**
     * @param userId 用户ID
     * @param cartId 购物车ID
     * @return ${return_type}
     * @description: 删除购物车
     * @date 2020/5/12 21:16
     */
    @PostMapping("/deleteCart")
    public CommonResult deleteCart(String userId, String cartId) {
        String user = (String) request.getSession().getAttribute("userId");
        if (StringUtils.isEmpty(user) || StringUtils.isEmpty(user) || !user.equals(userId))
            return new CommonResult(403, "无权限");
        else
            return new CommonResult(200, "删除成功", cartService.deleteCart(userId, cartId));
    }
}
