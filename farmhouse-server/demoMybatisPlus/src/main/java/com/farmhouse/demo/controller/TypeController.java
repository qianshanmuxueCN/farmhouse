package com.farmhouse.demo.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.farmhouse.demo.entity.CommonResult;
import com.farmhouse.demo.entity.Type;
import com.farmhouse.demo.entity.TypeDTO;
import com.farmhouse.demo.service.ITypeService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商品类别前端控制器
 * </p>
 *
 * @author 千山暮雪
 * @since 2020-04-03
 */
@Api("商品类别相关的接口")
@RestController
@RequestMapping("/commodity/type")
public class TypeController {
    @Autowired
    private ITypeService typeService;
    @Autowired
    private HttpServletRequest request;


    /**
     * @description: 新增一个类别
     * @date 2020/4/7 21:58
     */
    @PostMapping("/addType")
    public CommonResult addType(Type type) {
        return typeService.save(type) ? new CommonResult(200, "保存成功") : new CommonResult(400, "保存失败");
    }

    /**
     * @description: 分页查询
     * @date 2020/4/7 22:15
     */
    @GetMapping("/getTypeByPage")
    public CommonResult<IPage<Type>> getTypeByPage(TypeDTO typeDTO) {
        String shopId = (String) request.getSession().getAttribute("userId");
        IPage<Type> page = new Page<>();
        page.setCurrent(typeDTO.getPageNum()); //当前页
        page.setSize(typeDTO.getSize());    //每页条数
        IPage<Type> commodityIPage = typeService.getByPage(new Type(typeDTO).setShopId(shopId), page);
        return new CommonResult<IPage<Type>>(200, "查询成功", commodityIPage);
    }

    /**
     * @return
     * @description: 获取商品类别列表
     * @date 2020/4/12 22:37
     */
    @GetMapping("/getTypeList")
    public CommonResult<List<Map<String, Object>>> getTypeList() {
        String shopId = (String) request.getSession().getAttribute("userId");
        if (StringUtils.isEmpty(shopId)) {
            return new CommonResult<>(401, "无权限");
        }
        return new CommonResult<List<Map<String, Object>>>(200, "查询成功", typeService.getTypeList(shopId));
    }

    /***
     * 删除类别
     */
    @PostMapping("/deleteTypeById")
    public CommonResult deleteTypeById(@NotNull String typeId, @NotNull String shopId) {
        if (request.getSession().getAttribute("userId").equals(shopId))
            return typeService.deleteType(typeId) == 1 ? new CommonResult(200, "删除成功") : new CommonResult(400, "删除失败");
        else
            return new CommonResult(403, "删除失败");
    }

    /***
     * 修改类别
     */
    @PostMapping("/updateType")
    public CommonResult updateType(Type type) {
        if (ObjectUtils.isEmpty(type) || StringUtils.isEmpty(type.getTypeId())) {
            return new CommonResult(400, "参数异常");
        } else
            return typeService.updateType(type) == 1 ? new CommonResult(200, "修改成功") : new CommonResult(400, "修改失败");
    }

    /**
     * @description: 店铺主页获取商品类别列表
     */
    @GetMapping("/getTypeListByShop")
    public CommonResult<List<Map<String, Object>>> getTypeListByShop(String shopId) {
        if (StringUtils.isEmpty(shopId)) {
            return new CommonResult<>(401, "无权限");
        }
        return new CommonResult<>(200, "查询成功", typeService.getTypeList(shopId));
    }
}
