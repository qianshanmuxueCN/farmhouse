package com.farmhouse.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author
 * @title: CommonResult
 * @projectName cloud2020
 * @description: 通用返回对象
 * @date 2020/3/18 19:42
 */
@Data
//全参数构造函数的注释
@AllArgsConstructor
public class CommonResult<T> implements Serializable {
    private Integer code;
    private String message;
    private T data;

    public CommonResult(Integer code, String message) {
        this(code, message, null);
    }

}
