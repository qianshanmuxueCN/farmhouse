package com.farmhouse.demo.entity;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author 千山暮雪
 * @title: CartVO
 * @projectName demo
 * @description: 购物车列表VO
 * @date 2020/5/12 17:39
 */
@Data
public class CartVO {
    //商品数据
    private String commodityId;
    private String commodityTitle;//商品标题
    private BigDecimal price;//价格
    private String description;//描述
    private Integer isDiscount;//是否打折 0否，1是
    private BigDecimal discountPrice;//折扣价格
    private Integer inventory;//库存
    //图片数据
    private String imageUrl;
    //购物车数据
    private String cartId;
    private Integer number;
}
